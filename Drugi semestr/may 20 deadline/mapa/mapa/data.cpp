//
//  data.cpp
//  mapa
//
//  Created by Łukasz Michalak on 05/05/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include "data.hpp"
Addresses::Addresses(std::ifstream& file){
    std::string nazwa;
    for(;;){
        file>>nazwa;
        file>>_data[nazwa];
        if(file.eof()){
            break;
        }
    }
}

void Addresses::nowaPozycja(){
    std::cout<<"Prosze podac nazwe kontaktu: ";
    std::string nazwa,mail;
    getchar();
    getline(std::cin,nazwa);
    if(std::string::npos!=nazwa.find(' ')){
        system("clear");
        std::cout<<"Nazwa nie moza zawierac spacji!"<<std::endl;
        return;
    }
    std::map<std::string,std::string>::iterator itr=_data.find(nazwa);
    if(itr!=_data.end()){
        system("clear");
        std::cout<<"Podana nazwa jest juz zajeta!"<<std::endl;
        return;
    }
    std::cout<<"Prosze podac adres email: ";
    getline(std::cin,mail);
    if(std::string::npos!=mail.find(' ')){
        system("clear");
        std::cout<<"Email nie moza zawierac spacji!"<<std::endl;
        return;
    }
    _data[nazwa]=mail;
    system("clear");
    std::cout<<"Udalo sie dodac wybrany element!"<<std::endl;
}

void Addresses::wypisz(){
    system("clear");
    for(std::map<std::string,std::string>::iterator itr=_data.begin();itr!=_data.end();itr++){
        std::cout<<itr->first<<" -> "<<itr->second<<std::endl;
    }
}

void Addresses::usuwanie(){
    std::cout<<"Prosze podac nazwe kontaktu do usuniecia: ";
    std::string nazwa;
    getchar();
    getline(std::cin,nazwa);
    if(std::string::npos!=nazwa.find(' ')){
        system("clear");
        std::cout<<"Nazwa nie moza zawierac spacji!"<<std::endl;
        return;
    }
    std::map<std::string,std::string>::iterator itr=_data.find(nazwa);
    if(itr==_data.end()){
        system("clear");
        std::cout<<"Nie udalo sie wyszukac elementu do usuniecia!"<<std::endl;
    }
    else{
        _data.erase(itr);
        system("clear");
        std::cout<<"Udalo sie usunac wybrany element!"<<std::endl;
    }
}

void Addresses::zamiana(){
    std::cout<<"Prosze podac nazwe kontaktu do aktualizacji: ";
    std::string nazwa,mail;
    getchar();
    getline(std::cin,nazwa);
    if(std::string::npos!=nazwa.find(' ')){
        system("clear");
        std::cout<<"Nazwa nie moza zawierac spacji!"<<std::endl;
        return;
    }
    std::map<std::string,std::string>::iterator itr=_data.find(nazwa);
    if(itr==_data.end()){
        system("clear");
        std::cout<<"Nie udalo sie wyszukac elementu do aktualizacji!"<<std::endl;
    }
    else{
        std::cout<<"Prosze podac nowy adres emial: ";
        getline(std::cin,mail);
        if(std::string::npos!=mail.find(' ')){
            system("clear");
            std::cout<<"Email nie moza zawierac spacji!"<<std::endl;
            return;
        }
        itr->second=mail;
        system("clear");
        std::cout<<"Udalo sie zaktualizowac wybrany email!"<<std::endl;
    }
}

std::string Addresses::zapisywanie(){
    std::string return_value;
    for(std::map<std::string,std::string>::iterator itr=_data.begin();itr!=_data.end();itr++){
        return_value+=itr->first+" "+itr->second+" ";
    }
    return return_value;
}
