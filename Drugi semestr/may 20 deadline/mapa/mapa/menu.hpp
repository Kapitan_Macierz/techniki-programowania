//
//  menu.hpp
//  mapa
//
//  Created by Łukasz Michalak on 05/05/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#ifndef menu_hpp
#define menu_hpp

#include <stdio.h>
#include "data.hpp"
int menu(Addresses& book);
#endif /* menu_hpp */
