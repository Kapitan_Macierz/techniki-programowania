//
//  data.hpp
//  mapa
//
//  Created by Łukasz Michalak on 05/05/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#ifndef data_hpp
#define data_hpp

#include <stdio.h>
#include <fstream>
#include <map>
#include <string>
#include <iostream>


class Addresses{
public:
    Addresses(std::ifstream& file);
    void nowaPozycja();
    void wypisz();
    void usuwanie();
    void zamiana();
    std::string zapisywanie();
private:
    std::map<std::string,std::string> _data;
};

#endif /* data_hpp */
