//
//  menu.cpp
//  mapa
//
//  Created by Łukasz Michalak on 05/05/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include "menu.hpp"
int menu(Addresses& book){
    std::cout<<std::endl<<"Prosze wybrac opcje z menu: "<<std::endl
    <<"0 - Koniec dzialania na ksiazce adresowej"<<std::endl
    <<"1 - Dodanie nowej pozycji do ksiazki adresowej"<<std::endl
    <<"2 - Wypisanie calej ksiazki adresowej"<<std::endl
    <<"3 - Usuwanie wybranego elementu ksiazki adresowej"<<std::endl
    <<"4 - Aktualizacja emailu w ksiazce adresowej"<<std::endl
    ;
    int choice;
    std::cin>>choice;
    switch (choice) {
        case 1:
            book.nowaPozycja();
            break;
        case 2:
            book.wypisz();
            break;
        case 0:
            return 0; //stop
        case 3:
            book.usuwanie();
            break;
        case 4:
            book.zamiana();
            break;
        default:
            std::cout<<"Wybrano zla opcje!"<<std::endl;
            break;
    }
    return 1;//trzymanie petli
}
