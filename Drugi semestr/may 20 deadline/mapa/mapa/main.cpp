//
//  main.cpp
//  mapa
//
//  Created by Łukasz Michalak on 05/05/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include <iostream>
#include "data.hpp"
#include "menu.hpp"

using namespace std;

int main(int argc, const char * argv[]) {
    system("clear");
    cout<<"Prosze podac nazwe pliku: ";
    string nazwaPliku;
    cin>>nazwaPliku;
    ifstream plik(nazwaPliku);//read only
    if(!plik.is_open()){
        cout<<"Nie udało sie otwirzyc pliku o nazwie "<<nazwaPliku<<"!"<<endl;
        return 1;
    }  //szukanie pliku
    
    Addresses book(plik);
    plik.close(); //przepisywanie z pilku do mapy
    
    system("clear");
    int i;
    do{
        i=menu(book);
    }while(i);//menu
    system("clear");
    
    ofstream aktualizacja_pliku(nazwaPliku);
    aktualizacja_pliku<<book.zapisywanie();
    aktualizacja_pliku.close(); //zapis nowego do pliku
    
    return 0;
}


