//
//  mapa.hpp
//  szablon
//
//  Created by Łukasz Michalak on 08/05/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#ifndef mapa_hpp
#define mapa_hpp

#include <stdio.h>
#include <iostream>
#include <vector>
template<class Type>
struct Node{
    Node* left;
    Node* right;
    Type value;
    Type key;
};

template<class T>
class Map{
public:
    Map();
    ~Map();
    void addElement(T key,T element);
    void showAll();
    void deleteElement(T key);
private:
    Node<T>* _root;
    void _addElement(Node<T>** node,T element,T key);
    void _showAll(Node<T>** node);
    int _Element(Node<T>** node, T key,std::vector<T>& val,std::vector<T>& k);
    void _delete(Node<T>** node);

};


template <class T>
Map<T>::Map(){
    _root=NULL;
}

template <class T>
void Map<T>::addElement(T key,T element){
    _addElement(&_root, element, key);
}

template <class T>
void Map<T>::_addElement(Node<T>** node,T element,T key){
    if(*node!=NULL){
        if((*node)->key<key){
            _addElement(&((*node)->right), element, key);//wieksze w prawo
        }
        else if((*node)->key>key){
            _addElement(&((*node)->left), element, key);//mniejsze w lewo
        }
        else if((*node)->key==key){
            bool i=true; // do petli
            std::cout<<"Nie mozna dodac elementu, poniewaz klucze sie powtarzają!"<<std::endl;
            std::cout<<"Dla "<<key<<" wartosci stare: "<<(*node)->value<<" oraz nowe: "<<element<<std::endl;
            do{
                std::cout<<"Czy chcesz nadpisać istniejacy juz element? [Y/N]"<<std::endl;
                char wybor;
                std::cin>>wybor;
                switch (wybor) {
                    case 'Y':
                        (*node)->value=element;
                        system("clear");
                        std::cout<<"Element zostal nadpisany"<<std::endl;
                        i=false;
                        break;
                    case 'y':
                        (*node)->value=element;
                        system("clear");
                        std::cout<<"Element zostal nadpisany"<<std::endl;
                        i=false;
                        break;
                    case 'N':
                        system("clear");
                        std::cout<<"Element nie zostal nadpisany"<<std::endl;
                        i=false;
                        break;
                    case 'n':
                        system("clear");
                        std::cout<<"Element nie zostal nadpisany"<<std::endl;
                        i=false;
                        break;
                    default:
                        system("clear");
                        std::cout<<"Wybrano zla opcje"<<std::endl;
                        break;
                }
            }while(i);
            
        }
    }
    else{
        *node=new Node<T>;
        (*node)->left=NULL;
        (*node)->right=NULL;
        (*node)->key=key;
        (*node)->value=element;
        system("clear");
        std::cout<<"Element zostal dodany"<<std::endl;
    }
}

template<class T>
void Map<T>::showAll(){
    _showAll(&_root);
}

template<class T>
void Map<T>::_showAll(Node<T>** node){
    if(*node!=NULL){
        _showAll(&((*node)->left));
        std::cout<<(*node)->key<<" -> "<<(*node)->value<<std::endl;
        _showAll(&((*node)->right));
    }
}

template<class T>
Map<T>::~Map<T>(){
    _delete(&_root);
}

template<class T>
void Map<T>::_delete(Node<T> **node){
    if(*node!=NULL){
        _delete(&((*node)->right));
        _delete(&((*node)->left));
        delete (*node);
        (*node)=NULL;
    }
}


template <class T>
void Map<T>::deleteElement(T key){
    std::vector<T> val;
    std::vector<T> k;
    int i=_Element(&_root, key, val,k);//wpisuje wszystkie elementy poza tym do usuniecia i zwraca czy usunieto
    if(i){//jak usunieto
        _delete(&_root);
        for(int i=0;i<k.size();i++){//przepisz wszystkie wartosci do mapy od nowa do mapy
            addElement(k[i], val[i]);
            system("clear");
            std::cout<<"Element o kluczu "<<key<<" zostal usuniety"<<std::endl;
        }
    }
    else{//jak nie usunieto
        system("clear");
        std::cout<<"Element o kluczu "<<key<<" nie zostal znaleziony"<<std::endl;
        return;
    }
    
}

template<class T>
int Map<T>::_Element(Node<T> **node, T key,std::vector<T>& val,std::vector<T>& k){
    int i=0;//nie usunieto
    if(*node!=NULL){
        if(((*node)->key)!=key){//zapisz wartosci jak to nie jest element do usuniecia
            val.push_back((*node)->value);
            k.push_back((*node)->key);
        }else{
            i=1;//usunieto element
        }
        if(i==1){//jak usunieto to nie warto zapisywac co zwraca i tylko wartosci do wektora leca
            _Element(&(*node)->right, key, val,k);
            _Element(&(*node)->left, key, val,k);
        }else{
            i=_Element(&(*node)->right, key, val,k);//jak nie usunieto, to czy w prawym poddrzewie usunieto
            if(i==1){
                _Element(&(*node)->left, key, val,k);//jak tak to wtedy tu nie sprawdza returna
            }
            else{//jak nie to czy w lewym
                i=_Element(&(*node)->left, key, val,k);//i przy okazji wszystkie elementy wpisywane do wektorow
            }
        }
    }
    return i;
}

#endif /* mapa_hpp */
