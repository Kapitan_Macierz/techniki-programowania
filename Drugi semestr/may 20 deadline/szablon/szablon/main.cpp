//
//  main.cpp
//  szablon
//
//  Created by Łukasz Michalak on 08/05/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include <iostream>
#include <string>
#include "mapa.hpp"
#include <unistd.h>

void key(){
    std::cout<<"Nacisnij dowolny klawisz"<<std::endl;
    getchar();
    getchar();
}

int main(int argc, const char * argv[]) {
    
    Map<int> test;
    int k,e;
    
        do{
        system("clear");
        std::cout<<std::endl<<"Prosze wybrac operacje: "<<std::endl
        <<"0 - Zakoncz"<<std::endl
        <<"1 - Dodaj wartosc"<<std::endl
        <<"2 - Usun wartosc"<<std::endl
        <<"3 - Pokaz wszystkie elementy"<<std::endl
        ;
            int choice;
        std::cin>>choice;
        switch (choice) {
            case 1:
                std::cout<<"Prosze podac klucz: ";
                std::cin>>k;
                std::cout<<"Prosze podac element: ";
                std::cin>>e;
                test.addElement(k, e);
                key();
                break;
            case 2:
                std::cout<<"Prosze podac klucz: ";
                std::cin>>k;
                test.deleteElement(k);
                key();
                break;
            case 0:
                return 0;
            case 3:
                system("clear");
                test.showAll();
                key();
                break;
            default:
                std::cout<<"Wybrano zla opcje!"<<std::endl;
                break;
        }
        }while(1);
        
    return 1;
}
