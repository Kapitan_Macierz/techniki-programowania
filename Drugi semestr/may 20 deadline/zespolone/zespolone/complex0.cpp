//
//  complex0.cpp
//  zespolone
//
//  Created by Łukasz Michalak on 20/04/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include "complex0.hpp"
#include <math.h>

std::ostream& operator<<(std::ostream& out, Complex obiekt){
    if(obiekt._b>=0){
        out<<obiekt._a<<"+"<<obiekt._b<<"i";
    }
    else{
        out<<obiekt._a<<obiekt._b<<"i";
    }
    return out;
}

Complex::Complex(){
    _a=0;
    _b=0;
}
Complex::Complex(double a, double b){
    _a=a;
    _b=b;
}
Complex::Complex(const Complex& kopia){
    _a=kopia._a;
    _b=kopia._b;
}

void Complex::setZ(double a, double b){
    _a=a;
    _b=b;
}
void Complex::setRe(double a){
    _a=a;
}
void Complex::setIm(double a){
    _b=a;
}
double Complex::getIm(){
    return _b;
}
double Complex::getRe(){
    return _a;
}

Complex Complex::operator+(const Complex obiekt)const{
    Complex temp;
    temp._a=_a+obiekt._a;
    temp._b=_b+obiekt._b;
    return temp;
}

Complex Complex::operator+(double x)const{
    Complex temp;
    temp._a=x+_a;
    temp._b=_b;
    return temp;
}

Complex operator+(double x, const Complex obiekt){
    return obiekt+x;
}

Complex Complex::operator-(const Complex obiekt)const{
    Complex temp;
    temp._a=_a-obiekt._a;
    temp._b=_b-obiekt._b;
    return temp;
}

Complex Complex::operator-(double x)const{
    Complex temp;
    temp._a=_a-x;
    temp._b=_b;
    return temp;
}

Complex operator-(double x, const Complex obiekt){
    Complex temp(x-obiekt._a,-obiekt._b);
    return temp;
}

Complex Complex::operator*(const Complex obiekt)const{
    Complex temp(_a*obiekt._a-_b*obiekt._b,_a*obiekt._b+_b*obiekt._a);
    return temp;
}

Complex Complex::operator*(double x)const{
    Complex temp(_a*x,_b*x);
    return temp;
}

Complex operator*(double x, const Complex obiekt){
    return obiekt*x;
}

Complex conjugate(const Complex obiekt){
    Complex temp(obiekt._a,-obiekt._b);
    return temp;
}

double argument(const Complex obiekt){
    double Fi;
    if(obiekt._a>0){
        Fi=atan(obiekt._b/obiekt._a);
    }
    else{
        Fi=atan(obiekt._b/obiekt._a)+M_PI;
    }
    return Fi*360/(2*M_PI);
}

double mod(Complex obiekt){
    return sqrt(obiekt._a*obiekt._a + obiekt._b*obiekt._b);
}

Complex Complex::operator/(const Complex obiekt)const{
    Complex temp;
    temp._a=(_a*obiekt._a+_b*obiekt._b)/(obiekt._a*obiekt._a+obiekt._b*obiekt._b);
    temp._b=(obiekt._a*_b-_a*obiekt._b)/(obiekt._a*obiekt._a+obiekt._b*obiekt._b);
    return temp;
}

Complex Complex::operator/(double x)const{
    Complex temp(_a/x,_b/x);
    return temp;
}

Complex operator/(double x, const Complex obiekt){
    return obiekt/x;
}
