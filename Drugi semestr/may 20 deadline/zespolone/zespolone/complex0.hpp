//
//  complex0.hpp
//  zespolone
//
//  Created by Łukasz Michalak on 20/04/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#ifndef complex0_hpp
#define complex0_hpp

#include <stdio.h>
#include <iostream>
#include <string>


class Complex{
public:
    Complex();
    Complex(double a, double b);
    Complex(const Complex& kopia);
    friend std::ostream& operator<<(std::ostream& out, Complex obiekt);
    friend Complex conjugate(const Complex obiekt);
    friend double argument(const Complex obiekt);
    friend double mod(const Complex obiekt);

    
    Complex operator+(const Complex obiekt) const; // dwa obiekty
    Complex operator+(double x) const; //obiket plus liczba
    friend Complex operator+(double x,const Complex obiekt); //liczba plus obiekt
    
    Complex operator-(const Complex obiekt) const;
    Complex operator-(double x) const;
    friend Complex operator-(double x,const Complex obiekt);
    
    Complex operator*(const Complex obiekt) const;
    Complex operator*(double x) const;
    friend Complex operator*(double x,const Complex obiekt);
    
    Complex operator/(const Complex obiekt) const;
    Complex operator/(double x) const;
    friend Complex operator/(double x,const Complex obiekt);
    
    void setZ(double a, double b);
    void setRe(double);
    void setIm(double);
    double getRe();
    double getIm();
private:
    int _a; //real
    int _b; //imaginary
};



#endif /* complex0_hpp */
