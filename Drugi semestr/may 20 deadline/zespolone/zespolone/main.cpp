//
//  main.cpp
//  zespolone
//
//  Created by Łukasz Michalak on 20/04/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include <iostream>
#include "complex0.hpp"


using namespace std;
int main(int argc, const char * argv[]) {
    Complex a(2,2),b(2,3),c(a),d;
    double x=5;
    cout<<"a = "<<a<<endl<<"b = "<<b<<endl<<"c = "<<c<<endl<<"d = "<<d<<endl<<"x = "<<x<<endl;
    cout<<"a+x = "<<a+x<<endl;
    cout<<"x+a = "<<x+a<<endl;
    cout<<"a+b = "<<a+b<<endl;
    cout<<"a-x = "<<a-x<<endl;
    cout<<"x-a = "<<x-a<<endl;
    cout<<"a-b = "<<a-b<<endl;
    cout<<"a*x = "<<a*x<<endl;
    cout<<"x*a = "<<x*a<<endl;
    cout<<"a*b = "<<a*b<<endl;
    cout<<"a/x = "<<a/x<<endl;
    cout<<"x/a = "<<x/a<<endl;
    cout<<"a/b = "<<a/b<<endl;
    cout<<"sprzezenie a = "<<conjugate(a)<<endl;
    cout<<"argument a = "<<argument(a)<<endl;
    c.setZ(1, 10);
    cout<<"c po setZ = "<<c<<endl;
    cout<<"c getRe = "<<c.getRe()<<endl;
    cout<<"c getIm = "<<c.getIm()<<endl;
    c.setRe(4);
    cout<<"c po setRe = "<<c<<endl;
    c.setIm(3);
    cout<<"c po setIm = "<<c<<endl;
    
    cout<<"modul c= "<<mod(c)<<endl;

    
    return 0;
}
