//
//  Rectangle.hpp
//  figurki
//
//  Created by Łukasz Michalak on 23/04/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#ifndef Rectangle_hpp
#define Rectangle_hpp

#include <stdio.h>
#include "Figure.hpp"
class Rectangle:public Figure{
public:
    Rectangle();
    Rectangle(double a,double b);
    double area() const;
    std::ostream& print(std::ostream&)const;
    
    
private:
    double _a;
    double _b;
};
#endif /* Rectangle_hpp */
