//
//  Triangle.hpp
//  figurki
//
//  Created by Łukasz Michalak on 23/04/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#ifndef Triangle_hpp
#define Triangle_hpp

#include <stdio.h>
#include "Figure.hpp"
class Triangle:public Figure{
public:
    Triangle();
    Triangle(double a,double b, double c);
    double area() const;
    std::ostream& print(std::ostream&) const;
private:
    double _a;
    double _b;
    double _c;
};
#endif /* Triangle_hpp */
