//
//  Circle.hpp
//  figurki
//
//  Created by Łukasz Michalak on 23/04/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#ifndef Circle_hpp
#define Circle_hpp
#include "Figure.hpp"
#include <iostream>

class Circle:public Figure{
public:
    Circle();
    Circle(double a);
    double area() const;
    std::ostream& print(std::ostream&) const;
private:
    double _r;
};
#include <stdio.h>

#endif /* Circle_hpp */
