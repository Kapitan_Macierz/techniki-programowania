//
//  Rectangle.cpp
//  figurki
//
//  Created by Łukasz Michalak on 23/04/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include "Rectangle.hpp"
#include <cmath>
Rectangle::Rectangle(){
    _a=0;
    _b=0;
}

Rectangle::Rectangle(double a,double b){
    _a=a;
    _b=b;

}

double Rectangle::area()const{
    return _a*_b;
}

std::ostream& Rectangle::print(std::ostream& a)const{
    
    a<<" xxxxxxxxxxx"<<std::endl<<" x         x"<<std::endl<<" x         x"<<std::endl<<" xxxxxxxxxxx"<<std::endl;
    a<<"Dlugosci bokow: "<<_a<<" i "<<_b<<std::endl;
    return a;
}

