//
//  Figure.cpp
//  figurki
//
//  Created by Łukasz Michalak on 26/04/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include "Figure.hpp"
std::ostream& operator<<(std::ostream& out, Figure& f){
    return f.print(out);
}
