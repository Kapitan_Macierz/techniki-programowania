//
//  main.cpp
//  figurki
//
//  Created by Łukasz Michalak on 23/04/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include <iostream>
#include "Figure.hpp"
#include "Circle.hpp"
#include "Triangle.hpp"
#include "Rectangle.hpp"
using namespace std;

int main(int argc, const char * argv[]) {
    Figure** figures = new Figure*[3];
    figures[0] = new Circle(1);
    figures[1] = new Triangle(1,1,1);
    figures[2] = new Rectangle(2,3);
    for(int i=0; i<3; i++){
        cout << *figures[i] << endl;
    }
    cout << "Pola figur:" << endl;
    for(int i=0; i<3; i++){
        cout << figures[i]->area() << endl;
    }
    for(int i=0; i<3; i++){
        delete figures[i];
    }
    delete[] figures;

    return 0;
}
