//
//  Circle.cpp
//  figurki
//
//  Created by Łukasz Michalak on 23/04/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include "Circle.hpp"
#include <cmath>
#define PI 4*atan(1.0)
Circle::Circle(){
    _r=0;
}

Circle::Circle(double a){
    _r=a;
}

double Circle::area()const{
    return PI*_r*_r;
}

std::ostream& Circle::print(std::ostream& a)const{
    
    a<<"     xxx    "<<std::endl<<"  xx     xx "<<std::endl<<" x         x "<<std::endl<<" x         x "<<std::endl<<"  xx     xx "<<std::endl<<"     xxx    "<<std::endl;
    a<<"Promien: "<<_r<<std::endl;
    return a;
}
