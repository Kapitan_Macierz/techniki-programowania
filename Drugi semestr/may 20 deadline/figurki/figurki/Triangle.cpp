//
//  Triangle.cpp
//  figurki
//
//  Created by Łukasz Michalak on 23/04/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include "Triangle.hpp"
#include <cmath>
Triangle::Triangle(){
    _a=0;
    _b=0;
    _c=0;
}

Triangle::Triangle(double a,double b, double c){
    _a=a;
    _b=b;
    _c=c;
}

double Triangle::area()const{
    double l=(_a+_b+_c)/2;
    return sqrt(l*(l-_a)*(l-_b)*(l-_c));
}

std::ostream& Triangle::print(std::ostream& a)const{
    
    a<<"      x    "<<std::endl<<"    x   x  "<<std::endl<<"  x       x "<<std::endl<<" x x x x x x "<<std::endl;
    a<<"Dlugosci bokow: "<<_a<<", "<<_b<<" i "<<_c<<std::endl;
    return a;
    
}
