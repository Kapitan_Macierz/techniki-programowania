//
//  Figure.hpp
//  figurki
//
//  Created by Łukasz Michalak on 23/04/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#ifndef Figure_hpp
#define Figure_hpp

#include <stdio.h>
#include <iostream>

class Figure{
public:
    virtual double area() const = 0;
    virtual std::ostream& print(std::ostream&) const = 0;
};
std::ostream& operator<<(std::ostream& out, Figure& f);

#endif /* Figure_hpp */
