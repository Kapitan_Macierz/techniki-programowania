//
//  main.cpp
//  czwórki
//
//  Created by Łukasz Michalak on 19/03/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include <iostream>

using namespace std;

void ruch(int wysokosc,int **gra,int gracz,int dlugosc);
int wygrana(int** gra,int gracz,int wysokosc,int dlugosc);
void poka(int** gra,int wysokosc, int dlugosc);

int main() {
    int wysokosc,dlugosc;
    do{
    cout<<"Prosze podac wysokosc: ";
    cin>>wysokosc; //bo od 0
    cout<<"Prosze podac dlugosc: ";
    cin>>dlugosc; //bo od 0
    if(wysokosc<4||dlugosc<4){
        cout<<"Plansza musi miec conajmnej 4 na 4, zeby gra miala sens!"<<endl;
        continue;
    }
        break;
    }while(1);
        
    int **gra=new int* [wysokosc];
    for(int i=0;i<wysokosc;i++){
        gra[i]=new int [dlugosc];
    }
    for(int i=0;i<wysokosc;i++){
        for(int j=0;j<dlugosc;j++){
            gra[i][j]=0;
        }
    }
    poka(gra, wysokosc, dlugosc);
    for(int i=0,gracz=1;i<wysokosc*dlugosc;i++,gracz++){
        ruch(wysokosc,gra,gracz,dlugosc);
        system("clear");
        poka(gra, wysokosc, dlugosc);
        if(wygrana(gra, gracz, wysokosc, dlugosc)==1){
            cout<<"Gracz "<<gracz<<" wygral :D"<<endl;
            break;
        };
        if(gracz==2){
            gracz-=2;//zmiana graczy
        }
    }
    return 0;
}

void ruch(int wysokosc,int **gra,int gracz,int dlugosc){
    cout<<"Gracz "<<gracz<<endl;
    int kolumna,wpisanie=0;
    do{
    cout<<"Prosze podac kolumne do wrzucenia: ";
    cin>>kolumna;
        if(kolumna<=dlugosc&&kolumna>0){
    kolumna-=1;
    for(int i=0;i<wysokosc;i++){
        if(gra[i][kolumna]==0){
            gra[i][kolumna]=gracz;
            wpisanie=1;
            i=wysokosc;
        }
    }
    if(wpisanie==1){
        return;
    }
    else{
        cout<<"Nie ma juz miejsca! ";
        }
    }
        else{
            cout<<"Liczba musi byc wieksza od 0 oraz mniejsza od "<<dlugosc+1<<endl;
        }
    }while(1);
}
void poka(int** gra,int wysokosc, int dlugosc){
    for(int i=0;i<wysokosc;i++){
        for(int j=0;j<dlugosc;j++){
            cout<<gra[wysokosc-1-i][j]<<"  ";
        }
        cout<<endl<<endl;
    }
    for(int i=0;i<3*dlugosc-1;i++){
        cout<<"-";
    }
    cout<<endl;
    for(int i=0;i<dlugosc;i++){
        cout<<i+1<<"  ";
    }
    cout<<endl;
}
int wygrana(int** gra,int gracz,int wysokosc,int dlugosc){
    int prosto=0,skos_prawo=0,skos_lewo=0,pion=0;
    for(int i=0;i<wysokosc;i++){
        for(int j=0;j<dlugosc;j++){
            for(int k=0;k<4;k++){
                if(j+k<dlugosc){
                    if(gra[i][j+k]==gracz){
                        prosto++;
                    }
                }
                if(i+k<wysokosc){
                    if(gra[i+k][j]==gracz){
                        pion++;
                    }
                }
                if(j+k<dlugosc&&i+k<wysokosc){
                    if(gra[i+k][j+k]==gracz){
                        skos_prawo++;
                    }
                }
                if(j>=3&&i+k<wysokosc){
                    if(gra[i+k][j-k]==gracz){
                        skos_lewo++;
                    }
                }
            }
            if(prosto==4||skos_lewo==4||skos_prawo==4||pion==4){
                return 1; //wygrana
            }
            prosto=pion=skos_prawo=skos_lewo=0;
        }
    }
    return 0;
}
