//
//  main.cpp
//  kolko eminem
//
//  Created by Łukasz Michalak on 09/03/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include <iostream>
using namespace std;

enum plansza{nic,iks,kolo};

void wypisz_plansze(int tab[3][3]);
void ruch(int gracz,int tab[3][3]);
int wygrna(int gracz,int tab[3][3]);

int main() {
    int gra[3][3];
    for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
            gra[i][j]=plansza(nic);
        }
    }
    wypisz_plansze(gra);
    int stop=0,remis=0;
    do{
        cout<<"Gracz X"<<endl;
        ruch(plansza(iks),gra);
        remis++;
        system("clear");
        wypisz_plansze(gra);
        stop=wygrna(plansza(iks),gra);
        if(stop==1){
            return 0;
        }
        if(remis==9){
            cout<<"Remis! Nikt nie wygrał :c"<<endl;
            return 0;
        }
        cout<<"Gracz O"<<endl;
        ruch(plansza(kolo),gra);
        remis++;
        system("clear");
        wypisz_plansze(gra);
        stop=wygrna(plansza(kolo),gra);
        if(stop==1){
            return 0;
        }
    }while(1);

    
    return 0;
}

void wypisz_plansze(int tab[3][3]){
    char liczby_na_znaki[3][3];
    for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
            switch (tab[i][j]) {
                case plansza(nic):
                    liczby_na_znaki[i][j]=' ';
                    break;
                case plansza(iks):
                    liczby_na_znaki[i][j]='X';
                    break;
                case plansza(kolo):
                    liczby_na_znaki[i][j]='O';
                    break;
            }
        }
    }
    cout<<"     1   2   3"<<endl;
    cout<<"   -------------"<<endl;
    cout<<"1  | "<<liczby_na_znaki[0][0]<<" | "<<liczby_na_znaki[0][1]<<" | "<<liczby_na_znaki[0][2]<<" |"<<endl;
    cout<<"   -------------"<<endl;
    cout<<"2  | "<<liczby_na_znaki[1][0]<<" | "<<liczby_na_znaki[1][1]<<" | "<<liczby_na_znaki[1][2]<<" |"<<endl;
    cout<<"   -------------"<<endl;
    cout<<"3  | "<<liczby_na_znaki[2][0]<<" | "<<liczby_na_znaki[2][1]<<" | "<<liczby_na_znaki[2][2]<<" |"<<endl;
    cout<<"   -------------"<<endl;
    
}
void ruch(int gracz,int tab[3][3]){
    int wiersz,kolumna;
    int petla=1,duza_petla=1;
    cout<<"Prosze podac wspolrzedne ruchu:"<<endl;
    do{//na to czy pole zajete
        do{//na to czy dobra wspolrzedna
            cout<<"Wiersz: ";
            cin>>wiersz;
            if(wiersz==1||wiersz==2||wiersz==3){//czy pole zajete
                petla=0;
            }
            else{
                cout<<"Zla wsporzedna! Należy wpisać 1, 2 lub 3!"<<endl;
            }
        }while(petla==1);
        wiersz-=1;
        
        petla=1;
        do{
            cout<<"Kolumna: ";
            cin>>kolumna;
            if(kolumna==1||kolumna==2||kolumna==3){//czy pole zajete
                petla=0;
            }
            else{
                cout<<"Zla wsporzedna! Należy wpisać 1, 2 lub 3!"<<endl;
            }
        }while(petla==1);
        kolumna-=1; //bo numeracja od 0
        
        if(tab[wiersz][kolumna]==plansza(nic)){
            tab[wiersz][kolumna]=gracz;
            duza_petla=0;
        }
        else{
            cout<<"To pole jest juz zajte! Prosze wybrać inne!"<<endl;
        }
    }while(duza_petla==1);
    
}
int wygrna(int gracz,int tab[3][3]){
    char zwyciesca;
    if(gracz==1){
        zwyciesca='X';
    }
    else{
        zwyciesca='O';
    }
    if((tab[0][0]==gracz&&tab[1][1]==gracz&&tab[2][2]==gracz)||(tab[2][0]==gracz&&tab[1][1]==gracz&&tab[0][2]==gracz)){
        cout<<"Wygrał gracz "<<zwyciesca<<" :D"<<endl;
        return 1;
    }
    for(int i=0;i<3;i++){
        if((tab[i][0]==gracz&&tab[i][1]==gracz&&tab[i][2]==gracz)||(tab[0][i]==gracz&&tab[1][i]==gracz&&tab[2][i]==gracz)){
            cout<<"Wygrał gracz "<<zwyciesca<<" :D"<<endl;
            return 1;
        }
    }
    return 0;
}
