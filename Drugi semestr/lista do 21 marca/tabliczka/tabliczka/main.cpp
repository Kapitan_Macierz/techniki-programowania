//
//  main.cpp
//  tabliczka
//
//  Created by Łukasz Michalak on 08/03/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include <iostream>
#include <iomanip>
#define N 5
using namespace std;

int main() {
    int wielkosc;
    cout<<"Prosze podać wielkość tablicznki mnożenia: ";
    cin>>wielkosc;
    cout<<endl;
    wielkosc+=1; //dodatkowy wymiar na legende
    int **tabliczka=new int* [wielkosc];
    for(int i=0;i<wielkosc;i++){
        tabliczka[i]=new int [wielkosc];
    }
    for(int i=1;i<wielkosc;i++){//legenda
        tabliczka[0][i]=i;
        tabliczka[i][0]=i;
    }

    for(int i=1;i<wielkosc;i++){ //wartosci tabliczki mnozenia
        for(int j=1;j<wielkosc;j++){
            tabliczka[i][j]=i*j;
        }
    }
    for(int i=0;i<wielkosc;i++){
        for(int j=0;j<wielkosc;j++){
            cout<<setw(N)<<tabliczka[i][j];
            if(j==0){
                cout<<" |";
            }
        }
        if(i==0){
            cout<<endl;
            for(int k=0;k<N*wielkosc+2;k++){
                cout<<"-";
            }
        }
        cout<<endl;
    }
    for(int i=0;i<wielkosc;i++){
        delete[] tabliczka[i];
    }
    delete[] tabliczka;
    return 0;
}
