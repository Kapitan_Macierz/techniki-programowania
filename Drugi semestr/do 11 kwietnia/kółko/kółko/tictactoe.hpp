//
//  tictactoe.hpp
//  kółko
//
//  Created by Łukasz Michalak on 28/03/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#ifndef tictactoe_hpp
#define tictactoe_hpp

#include <stdio.h>
enum plansza{nic,iks,kolo};

class tictactoe{
public:
    tictactoe();
    void wypisz_plansze();
    void ruch(plansza gracz);
    int wygrana(plansza gracz);
private:
    plansza _gra[3][3];
};
#endif /* tictactoe_hpp */
