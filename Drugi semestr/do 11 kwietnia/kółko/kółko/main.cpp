//
//  main.cpp
//  kółko
//
//  Created by Łukasz Michalak on 28/03/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include <iostream>
#include "tictactoe.hpp"
using namespace std;

int main(void) {
    tictactoe one;
    one.wypisz_plansze();
    int stop=0,remis=0;
    do{
        cout<<"Gracz X"<<endl;
        one.ruch(iks);
        remis++;
        system("clear");
        one.wypisz_plansze();
        stop=one.wygrana(iks);
        if(stop==1){
            return 0;
        }
        if(remis==9){
            cout<<"Remis! Nikt nie wygrał :c"<<endl;
            return 0;
        }
        cout<<"Gracz O"<<endl;
        one.ruch(kolo);
        remis++;
        system("clear");
        one.wypisz_plansze();
        stop=one.wygrana(kolo);
        if(stop==1){
            return 0;
        }
    }while(1);

    
    return 0;
}


