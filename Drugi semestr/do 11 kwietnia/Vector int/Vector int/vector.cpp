//
//  vector.cpp
//  Vector int
//
//  Created by Łukasz Michalak on 29/03/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include "vector.hpp"
#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

VectorInt::VectorInt()
{
    _tab=new int [16];
    _dlugosc=16;
    for(int i=0;i<_dlugosc;i++){
        _tab[i]=0;
    }
}

VectorInt::VectorInt(int liczba)
{
    _tab=new int [liczba];
    _dlugosc=liczba;
    for(int i=0;i<_dlugosc;i++){
        _tab[i]=0;
    }
}

VectorInt::VectorInt(const VectorInt& orgnial){
    _tab=new int[orgnial._dlugosc];
    for(int i=0;i<orgnial._dlugosc;i++){
        _tab[i]=orgnial._tab[i];
    }
}

int VectorInt::at(int indeks){
    return _tab[indeks];
}

void VectorInt::insert(int indeks, int wartosc){
    if(indeks<0||indeks>=_dlugosc){
        cout<<"Nieprawidłowy indeks!"<<endl;
    }
    else{
    _tab[indeks]=wartosc;
    }
}

VectorInt& VectorInt::operator= (const VectorInt& orginal){
    if(_dlugosc<orginal._dlugosc){
        _powiekszenie(orginal._dlugosc-_dlugosc);
    }
    for(int i=0;i<_dlugosc;i++){
        this->_tab[i]=orginal._tab[i];
    }

    return *this;
}

VectorInt::~VectorInt(){
    delete[] _tab;
}

void VectorInt::pushBack(int wartosc){
    int zrobione=0;
    do{
    for(int i=0;i<_dlugosc;i++){
        if(_tab[i]==0){
            _tab[i]=wartosc;
            zrobione=1;
            return;
        }
    }
    if(zrobione==0){
        cout<<"Breaklo miejsca"<<endl;
        _powiekszenie(_dlugosc/2);
    }
    }while(1);
}

void VectorInt::popBack(){
    int zrobione=0;
    for(int i=_dlugosc-1;i>=0;i--){
        if(_tab[i]!=0){
            _tab[i]=0;
            zrobione=1;
            break;
        }
    }
    if(zrobione==0){
        cout<<"Nie można bylo usunąć ostatniego elementu, poniewaz caly wektor byl juz pusty!"<<endl;
    }
}

void VectorInt::_powiekszenie(int ekstra){
    int* nowaTablica=new int[_dlugosc+ekstra];
    for(int i=0;i<_dlugosc;i++){
        nowaTablica[i]=_tab[i];
    }
    for(int i=_dlugosc;i<_dlugosc+ekstra;i++){
        nowaTablica[i]=0;
    }
    _dlugosc=_dlugosc+ekstra;
    delete[] _tab;
    _tab=nowaTablica;
    if(ekstra>=0){ // bo do zmniejszania tez uzywana 
    cout<<"Powiększono wektor o "<<ekstra<<endl;
    }
}

void VectorInt::shrinkToFit(){
    int j=0;
    int* nowaTablica=new int[_dlugosc];
    for(int i=0;i<_dlugosc;i++){
            if(_tab[i]!=0){
                nowaTablica[j]=_tab[i];
                j++;
            }
    }
    _powiekszenie(j-_dlugosc);
    delete[] _tab;
    _tab=nowaTablica;
}

std::ostream& operator<<(std::ostream& out, VectorInt& p){
    return out<<p.overload()<<endl;
}

string VectorInt::overload(){
    return _overload(0);
}

string VectorInt::_overload(int liczba){
    if(liczba==_dlugosc){
        return " ";
    }
    string pokaz;
    pokaz=to_string(at(liczba))+" "+_overload(liczba+1);
    return pokaz;
}

void VectorInt::clear(){
    for(int i=0;i<_dlugosc;i++){
        _tab[i]=0;
    }
}

int VectorInt::size(){
    int j=0;
    for(int i=0;i<_dlugosc;i++){
        if(_tab[i]!=0){
            j++;
        }
    }
    return j;
}

int VectorInt::capacity(){
    int j=0;
    for(int i=0;i<_dlugosc;i++){
        if(_tab[i]!=0){
            j++;
        }
    }
    return _dlugosc-j;
}
