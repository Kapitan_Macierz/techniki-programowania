//
//  vector.hpp
//  Vector int
//
//  Created by Łukasz Michalak on 29/03/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#ifndef vector_hpp
#define vector_hpp

#include <stdio.h>
#include <iostream>
#include <string>
class VectorInt{
public:
    VectorInt();
    VectorInt(int liczba);
    VectorInt(const VectorInt& orginal);
    ~VectorInt();
    int at(int indeks);
    void insert(int indeks,int wartosc);
    void pushBack(int wartosc);
    void popBack();
    VectorInt& operator= (const VectorInt& orginal);
    void shrinkToFit();
    std::string overload();
    void clear();
    int size();
    int capacity();
private:
    int* _tab;
    void _powiekszenie(int ekstra);
    int _dlugosc;
    std::string _overload(int liczba);
};

std::ostream& operator<<(std::ostream& out, VectorInt& p);
#endif /* vector_hpp */
