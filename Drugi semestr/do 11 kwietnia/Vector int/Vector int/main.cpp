//
//  main.cpp
//  Vector int
//
//  Created by Łukasz Michalak on 29/03/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include <iostream>
#include "vector.hpp"


using namespace std;

int main(void) {
    VectorInt podstawowy;
    VectorInt wektor(17);
    cout<<podstawowy;
    cout<<wektor;
    for(int i=0;i<19;i++){
        wektor.insert(i, i+1);
    }
    cout<<wektor;
    cout<<wektor.capacity()<<endl;
    cout<<wektor.size()<<endl;
    wektor.clear();
    cout<<wektor;
    cout<<wektor.capacity()<<endl;
    cout<<wektor.size()<<endl;
    wektor.insert(4, 4);
    wektor.insert(2, 7);
    cout<<wektor;
    cout<<wektor.capacity()<<endl;
    cout<<wektor.size()<<endl;
    wektor.shrinkToFit();
    cout<<wektor;
    wektor.popBack();
    cout<<wektor;
    wektor.popBack();
    cout<<wektor;
    wektor.popBack();
    wektor.pushBack(5);
    cout<<wektor;
    wektor.pushBack(6);
    cout<<wektor;
    wektor.pushBack(7);
    cout<<wektor;
    wektor.pushBack(4);
    cout<<wektor;
    wektor.pushBack(9);
    cout<<wektor;
    cout<<wektor.at(2)<<endl;
    return 0;
}
