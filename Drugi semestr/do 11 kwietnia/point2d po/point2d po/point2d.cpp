//
//  point2d.cpp
//  point2d po
//
//  Created by Łukasz Michalak on 25/03/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include "point2d.hpp"
#include <cmath>
#include <iostream>
#include <stdio.h>
#define M_PI 4*atan(1.)

Point2d::Point2d():
_r(0),
_fi(0)
{}

Point2d::Point2d(double x,double y):
_r(sqrt(x*x+y*y)),
_fi(liczenieFi(x, y))
{}
    
Point2d::Point2d(const Point2d& kopia)//konstruktor kopiujacy; przy tworzeniu nowego daje takie same
: _r(kopia._r)
, _fi(kopia._fi)
{}

Point2d& Point2d::operator= (const Point2d& kopia){ // operator przypisania; jak juz istnieje to se kopiuje
    this->_r = kopia._r;
    this->_fi = kopia._fi;
    //operator zwraca "ten" obiekt, aby można było
    //wykonać wielokrotne przypisanie
    return *this;
}

double Point2d::getX(){
    return _r*cos(_fi);
}

double Point2d::getY(){
    return _r*sin(_fi);
}

double Point2d::getR(){
    return _r;
}

double Point2d::getFi(){
    return _fi/((2*M_PI)/360);
}
void Point2d::jednokladnosc(double k){
    _r=_r*k;
}
void Point2d::obracanie(double kat){
    _fi=_fi+(kat*2*M_PI)/360;
}

void Point2d::setXY(double x, double y){
    _r=sqrt(x*x+y*y);
    _fi=atan(y/x);
}

void Point2d::setRFi(double r, double fi){
    _r=r;
    _fi=(fi*2*M_PI)/360;
}

double Point2d::liczenieFi(double x, double y){
    double Fi;
    if(x>0){
        Fi=atan(y/x);
    }
    else{
        Fi=atan(y/x)+M_PI;
    }
    return Fi;
}



//przeciążony operator<< dla wypisywania
std::ostream& operator<<(std::ostream& out, Point2d& p){
    return out << "[" << p.getX() << ", " << p.getY() << "]";
}
