//
//  point2d.hpp
//  point2d po
//
//  Created by Łukasz Michalak on 25/03/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#ifndef point2d_hpp
#define point2d_hpp

#include <stdio.h>
#include <iostream>
class Point2d{
public:
    Point2d(); // konstruktor podstawowy
    Point2d(double, double); // konstruktor z lista
    Point2d(const Point2d& kopia); //konstruktor kopiujacy
    Point2d& operator= (const Point2d& kopia); //przeciążony operator przypisania
    double getX();//getery wyciagaja z klasy
    double getY();
    double getR();
    double getFi();
    void jednokladnosc(double k);
    void obracanie (double kat);
    void setXY(double x,double y);//setery wkladaja do klasy
    void setRFi(double r, double fi);
private:
    double liczenieFi(double x,double y);
    double _r;
    double _fi;
};

//deklaracja przeciążonego operatora <<
//na potrzeby wyświetlania współrzędnych punktu
std::ostream& operator<<(std::ostream& out, Point2d& p);
#endif /* point2d_hpp */
