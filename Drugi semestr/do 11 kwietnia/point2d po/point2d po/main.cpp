//
//  main.cpp
//  point2d po
//
//  Created by Łukasz Michalak on 25/03/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

/*Program demonstrujący możliwości
 klasy Point2d.*/
#include <iostream>
#include "point2d.hpp"
#include <cmath>

using namespace std;

int main(void){
    Point2d a; //konstruktor domyślny
    cout<<"a = "<<a<<endl;
    a.setXY(3, -4); // ustawianie podajac x i y kartezjanskie
    cout<<"a = "<<a<<endl;
    a.setRFi(sqrt(2), 45); // ustawianie podajac dlugosc r i kat w stopniach
    cout<<"a = "<<a<<endl;
    cout<<"x od a: "<<a.getX()<<endl; // zwraca X
    cout<<"y od a: "<<a.getY()<<endl;// zwraca Y
    cout<<"r od a: "<<a.getR()<<endl; // zwraca R
    cout<<"phi od a: "<<a.getFi()<<endl;// zwraca kat w stopniach
    Point2d b(1,1); //konstruktor z lista argumentow
    cout<<"b = "<<b<<endl;
    Point2d c(b); //konstruktor kopiujący
    cout<<"c = "<<c<<endl;
    c.obracanie(90); //obrot o 90 stopni
    cout<<"c = "<<c<<endl;
    c.jednokladnosc(-4); // jednokladnosc wzgledem poczatku ukladu wspolrzednych
    cout<<"c = "<<c<<endl;
    c=b;
    cout<<"c = "<<c<<endl;//przeciazony operator przypisania

    return 0;
}

