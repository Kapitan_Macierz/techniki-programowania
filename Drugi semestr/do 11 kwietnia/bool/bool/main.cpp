//
//  main.cpp
//  bool
//
//  Created by Łukasz Michalak on 31/03/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include <iostream>
#include <string>
using namespace std;

bool match(const string& pattern, const string& s);


int main() {
    string pattern="*dsad",s="dsad";
    
    if(match(pattern,s)){
        cout<<"Zgadza sie"<<endl;
    }
    else{
        cout<<"Nie zgadza sie"<<endl;
    }
     
    return 0;
}

bool match(const string& pattern, const string& s){
    if(pattern.length()>s.length()+1){//jak jest ktorszy
        return 0;
    }
    if(pattern.empty()&&pattern.length()==s.length()){// jak pattern pusty o oba sa dlugosci 0 to koniec
        return 1;
    }
    if(pattern.empty()&&pattern.length()!=s.length()){
        return 0;
    }
    if(pattern.find_first_of('*')>0||pattern.find_first_of('*')==-1){ //jak gwizdka daleko albo jej nie ma to normalnie porównanie
        if(pattern.at(0)==s.at(0)||pattern.at(0)=='?'){
            return match(pattern.substr(1,pattern.length()), s.substr(1,s.length()));
        }
        else{
            return 0;
        }
    }else if(pattern.find_first_of('*')==0){ //jak giwazdka jest na 0
        if(pattern.length()==1){// i tylko giwazda jest w ciagu
            return 1;
        }
        if(pattern.at(1)==s.at(0)){//sprawdz czy ten przed gwiazdka i zerowy 0 sie zgadzaja
            return match(pattern.substr(1,pattern.length()), s);//jak tak to wyrównaj
        }
        else{
            return match(pattern, s.substr(1,s.length()));
        }
    }
    
    return 0;
}
