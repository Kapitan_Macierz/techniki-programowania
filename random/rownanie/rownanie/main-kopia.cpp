#include <iostream>
#include <iomanip>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>

using namespace std;

/*Definicja funkcji obliczającej wektor prawych stron układu równań*/
int
func (double t, const double y[], double f[],
      void *params)
{
    (void)(t); /* avoid unused parameter warning */
    double mu = *(double *)params;
    f[0] = y[1];
    f[1] = -y[0] - mu*y[1]*(y[0]*y[0] - 1);
    return GSL_SUCCESS;
}

/*Definicja funkcji obliczającej macierz Jacobiego*/
int
jac (double t, const double y[], double *dfdy,
     double dfdt[], void *params)
{
    (void)(t); /* avoid unused parameter warning */
    double mu = *(double *)params;
    gsl_matrix_view dfdy_mat
    = gsl_matrix_view_array (dfdy, 2, 2);
    gsl_matrix * m = &dfdy_mat.matrix;
    gsl_matrix_set (m, 0, 0, 0.0);
    gsl_matrix_set (m, 0, 1, 1.0);
    gsl_matrix_set (m, 1, 0, -2.0*mu*y[0]*y[1] - 1.0);
    gsl_matrix_set (m, 1, 1, -mu*(y[0]*y[0] - 1.0));
    dfdt[0] = 0.0;
    dfdt[1] = 0.0;
    return GSL_SUCCESS;
}

int
main (void)
{
    double mu = 10;
    /*Definicja układu równań, w strukturze znajdują się kolejno:
     - funkcja obliczająca prawe strony równania,
     - funkcja obliczająca macierz Jacobiego,
     - wymiar układu,
     - parametry.*/
    gsl_odeiv2_system sys = {func, jac, 2, &mu};
    
    /*Alokacja struktury sterującej rozwiązywaniem układu równań.
     Argumentami są:
     - struktura przechowująca układ równań,
     - określenie rodzaju kroku (w tym przypadku metoda Rungego-Kutty
     Prince'a-Dormanda rzędu 8/9),
     - początkowa wartość kroku,
     - dokładność względna,
     - dokładność bezwzględna.*/
    
    gsl_odeiv2_driver * d =
    gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rk8pd,
                                   1e-6, 1e-6, 0.0);
    double t = 0.0, t1 = 100.0;
    double y[2] = { 1.0, 0.0 };
    
    cout << setprecision(5) << scientific;
    
    for (int i = 1; i <= 100; i++)
    {
        double ti = i * t1 / 100.0;
        /*Przejście sterownikiem d od aktualnej wartości t
         do wartości ti. Wartości początkowe w y zastąpione są
         wartościami końcowymi.*/
        int status = gsl_odeiv2_driver_apply (d, &t, ti, y);
        
        if (status != GSL_SUCCESS)
        {
            cout << "error, return value=" << status << endl;
            break;
        }
        
        cout << t << " " << y[0] << " " << y[1] << endl;
    }
    
    gsl_odeiv2_driver_free (d);
    return 0;
}
