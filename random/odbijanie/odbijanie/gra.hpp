//
//  gra.hpp
//  odbijanie
//
//  Created by Łukasz Michalak on 08/04/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#ifndef gra_hpp
#define gra_hpp

#include <stdio.h>

struct pozycja{
    int wiersz;
    int kolumna;
};

class Arkanoid{
public:
    Arkanoid();
    void zabawa();
private:
    void _punkty();
    void _wypisz();
    void _bicie();
    int _score=0;
    pozycja _pilka;
    pozycja _bedzieBite;
    void _ufo();
    int _x=1;
    int _y=1;
    int _wysokosc =17;
    int _dlugosc =29;
    char _plansza[17][29];
};

#endif /* gra_hpp */
