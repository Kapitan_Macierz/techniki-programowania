//
//  gra.cpp
//  odbijanie
//
//  Created by Łukasz Michalak on 08/04/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//

#include "gra.hpp"
#include <iostream>

Arkanoid::Arkanoid(){
    for(int i=1;i<_wysokosc-1;i++){
        for(int j=0;j<_dlugosc;j++){
            if(j==0||j==_dlugosc-1){
                _plansza[i][j]='#';
                
            }
            else{
                _plansza[i][j]=' ';
            }
        }
    }
    for(int j=0;j<_dlugosc;j++){
        _plansza[0][j]='#';
    }
    for(int j=0;j<_dlugosc;j++){
        _plansza[_wysokosc-1][j]='#';
    }
#include <time.h>
    //srand (time(NULL));
    _pilka.kolumna=24;//rand()%_dlugosc/3+6;
    _pilka.wiersz=_wysokosc/2-1;
    _plansza[_pilka.wiersz][_pilka.kolumna]='O';
    
}

void Arkanoid::_wypisz(){
    for(int i=0;i<_wysokosc;i++){
        for(int j=0;j<_dlugosc;j++){
            std::cout<<_plansza[i][j];
        }
        std::cout<<std::endl;
    }
}

void Arkanoid::zabawa(){
    _punkty();
    _wypisz();

    for(;;){
        _ufo();
        _bicie();
        _wypisz();

        std::cout<<_score<<" "<<_x<<" "<<_y;
        getchar();
    }
}

void Arkanoid::_ufo(){
    if(_plansza[_pilka.wiersz+_y][_pilka.kolumna+_x]==' '){
        _plansza[_pilka.wiersz][_pilka.kolumna]=' ';
        _pilka.wiersz+=_y;
        _pilka.kolumna+=_x;
        _plansza[_pilka.wiersz][_pilka.kolumna]='O';
    }else{
        _bedzieBite.wiersz=_pilka.wiersz+_y;
        _bedzieBite.kolumna=_pilka.kolumna;
        if(_plansza[_pilka.wiersz+_y][_pilka.kolumna]!=' '){
            if(_y==1){
                _y=-1;
            }
            else{
                _y=1;
            }
        }
        if(_plansza[_pilka.wiersz][_pilka.kolumna+_x]!=' '){
            if(_x==1){
                _x=-1;
            }
            else{
                _x=1;
            }
        }
        _plansza[_pilka.wiersz][_pilka.kolumna]=' ';
        
        _pilka.wiersz+=_y;
        _pilka.kolumna+=_x;
        _plansza[_pilka.wiersz][_pilka.kolumna]='O';

    }
}

void Arkanoid::_punkty(){
    for(int i=1;i<5;i++){
        for(int j=1;j<_dlugosc-1;j+=1){
            _plansza[i][j]='x';
            //_plansza[i][j+1]=' ';

        }
    }
}

void Arkanoid::_bicie(){
    if(_plansza[_bedzieBite.wiersz][_bedzieBite.kolumna]=='x'){
        _plansza[_bedzieBite.wiersz][_bedzieBite.kolumna]=' ';
        _score++;
    }
}
