//
//  main.c
//  tablicowanie
//
//  Created by Łukasz Michalak on 28/11/2018.
//

#include <stdio.h>

int main(){
    int n;
    float i=0.99,tablica[6];
    printf("Wartosci tego wielomianu obliczone odpowiednio sposobem pierwszym i drugim to:\n");
    for (n=0;n<5;n+=2){
        tablica[n]=(i-1)*(i-1)*(i-1)*(i-1)*(i-1)*(i-1)*(i-1);
        tablica[n+1]=i*i*i*i*i*i*i-7*i*i*i*i*i*i+21*i*i*i*i*i-35*i*i*i*i+35*i*i*i-21*i*i+7*i-1;
        printf("- dla %.2f to %17.14f  i %17.14f\n",i,tablica[n],tablica[n+1]);
        i+=0.01;
    }
    return 0;
}
