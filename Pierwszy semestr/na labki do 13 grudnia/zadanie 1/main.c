#include <stdio.h>
#include <stdlib.h>
#include <float.h>

int main()
{
    printf("Najmniejsze liczby reprezentowane w arytmetykach zmiennoprzecikowych to dla:\n");
    printf("%7s %e\n%7s %e","Float:",FLT_MIN,"Double:",DBL_MIN);
    return 0;
}
