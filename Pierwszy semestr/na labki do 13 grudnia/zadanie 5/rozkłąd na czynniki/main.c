//
//  main.c
//  rozkłąd na czynniki
//
//  Created by Łukasz Michalak on 11/11/2018.
//  Copyright © 2018 Łukasz Michalak. All rights reserved.
//
#include <stdio.h>
#include <stdlib.h>

int main()
{
    int liczba,dzielenie=2;
    printf("Prosze podac liczbe do rozlozenia na czynniki pierwsze: ");
    scanf("%d",&liczba);
    if (liczba==0)
        printf("Liczba zero ma nieskonczenie wiele dzielnikow!\n");
    else if (liczba==1)
        printf("Jeden to jeden, nic więcej!\n"); //sprawdzenie specyficznych libcz 1 i 0
    else{
        while(liczba!=1){
            if(liczba%dzielenie!=0) // jak reszta równa zero to podzielna i dzielimy jak nie to zwiekszamy dzielnik
                dzielenie++;
            else{
                liczba=liczba/dzielenie;
                printf("%d\n",dzielenie);
            }
        }
    }
    return 0;
    
}
