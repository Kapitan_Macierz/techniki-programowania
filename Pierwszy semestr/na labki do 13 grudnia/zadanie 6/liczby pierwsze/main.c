//
//  main.c
//  liczby pierwsze
//
//  Created by Łukasz Michalak on 11/11/2018.
//

#include <stdio.h>
#include <malloc.h>
int main(){
    int liczbaUzytkownika=1,k,i,dzielnik=2,numerTablicy=2,*tablicaLiczb;
    printf("Prosze podać liczbe do której wypisane zostana liczby pierwsze: ");
    scanf("%d",&liczbaUzytkownika); //prosi o ograniczenie z góry
    tablicaLiczb=malloc(liczbaUzytkownika+1 * sizeof(*tablicaLiczb)); //tablica z liczbami do sprawdzania która ma wielkosc zalezna od ograniczenia z góry uzytkownika
    for(i=0;i<=liczbaUzytkownika;i++) /*generalnie to dziala tak ze jest tworzona tablica wielkosci podanej przez uzytkownika i wypełnia ja
    liczbami po kolei, potem dzieli kolejne wyrazy zaczynajac od 2 i gdzy reszta z dzielenia jest rowna zero to znaczy ze liczba jest podzielna,
    nie jest pierwsza wiec zeby odróznić podzielne to wszystkim podzielnim przypisuje wartosc 0, gdy skonczy to leci od nowa ze sprawdzaniem
    czy liczby sa podzielne i podstawia zera tylko tym razem dzieli przez najmniejsza liczbe w tablicy ktora nie jest zerem, wszystko konczy sie
    na liczbie ograniczajacej */
    {
        tablicaLiczb[i]=i;
    }
    for(i=0;i<liczbaUzytkownika;i++){
        numerTablicy=2+i;  //reset numer elementu znowu do poczatku +1
        if(tablicaLiczb[numerTablicy]!=0){ //jesli wartosc tablicy dla nastepnego numeru nie jest 0 to jest to teraz przez nia musimy dzielic
            dzielnik=numerTablicy; //zamienia dzielnik na kolenja liczbe pierwszą
            printf("%d\n",dzielnik);
        }
        for(k=2;k<liczbaUzytkownika;k++)
        {
            if(tablicaLiczb[numerTablicy]%dzielnik==0){ // jesli reszta z dzielenia jest równa zero dla elementu to jest podzielny i to nie jest pierwsza liczba
                tablicaLiczb[numerTablicy]=0; //przypisuje temu elementowi wartosc 0 zeby go odróznić
                numerTablicy++;
            }
            else{
                numerTablicy++;
            }
        }
    }
    free(tablicaLiczb);
return 0;
}
