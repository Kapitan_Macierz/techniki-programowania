//
//  na labki do 13 grudnia
//
//  Łukasz Michalak 03/11/2018
//

#include <stdio.h>
#include <malloc/_malloc.h>

int main()
{
    int stopien=0,i,n=0;
    float x,wynik=0,*wspolczynniki;
    printf("Prosze podac stopien wielomianu: ");
    scanf("%d",&stopien);
    wspolczynniki=malloc(stopien+1 * sizeof(*wspolczynniki)); //tablica ktora dostosowuje swoja dłguość do stopnia wielomianu podanego przez urzytkowanika
    printf("Prosze podac argument rzeczywisty x: ");
    scanf("%f",&x);
    printf("Prosze podac kolejne wspolczynniki wielomianu w kolejnosci od tego przy najwiekszej potedze:\n");
    for(i=0;i<stopien+1;i++)
    {
        scanf("%f",&wspolczynniki[i]); //wpisywanie wspolczynnikow
    }
    i=0;
    while(n<stopien)  //obliczanie wartosci wielomianu wedlug wzoru (((ax+b)x+c)x+d)x+e itd
    {
    if(wynik==0)
        {
        wynik=wspolczynniki[i]*x+wspolczynniki[i+1];
        i=i+2;
        n++;
        }
    else
        {
        wynik=wynik*x+wspolczynniki[i];
        i++;
        n++;
        }
    }
    free(wspolczynniki); //czyszczenie
    if (stopien==0)
        printf("Wynik to %.2f\n",wspolczynniki[0]);
    else
        printf("Wynik to %.2f\n",wynik);


    return 0;
}
