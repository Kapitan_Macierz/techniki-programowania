//
//  main.c
//  to długie z podpunkatmi
//
//Łukasz Michalak on 28/11/2018.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <malloc/malloc.h>
#include "Header.h"

int main(){
int i,dlugosc,*ciag,akcja,powtarzanieMenu=1,powtarzanieCalosci=1,zmianaCiagu=0,pytanie=1;
do{
    powtarzanieCalosci=1;
    printf("\nProsze podac liczbe wyrazow ciagu: ");
    scanf("%d",&dlugosc);
    ciag=malloc(dlugosc * sizeof(*ciag));
    printf("Prosze podac wyrazy ciągu: \n");
    for(i=0;i<dlugosc;i++){
        scanf("%d",&ciag[i]);
    }
    do{
    powtarzanieMenu=1;
    printf("\nProsze wybrac ktora akcja ma byc przeprowadzona na podanym ciagu: \n");
    printf("1 - wyznaczenie minimum\n2 - wyznaczenie maksimum\n3 - obliczanie sredniej\n4 - wyznaczenie mediany\n5 - obliczanie odchylenia standardowego\n0 - wyjscie\n");
    scanf("%d",&akcja);
    switch (akcja) {
        case 1:
            printf("\nMinimum tego ciagu to: %d\n",minimum(ciag, dlugosc));
            break;
        case 2:
            printf("\nMaksimum tego ciagu to: %d\n",max(ciag, dlugosc));
            break;
        case 3:
            printf("\nSrednia tego ciagu to: %.2f\n",srednia(ciag, dlugosc));
            break;
        case 4:
            if(dlugosc%2==0){
                printf("\nMediana tego ciagu to: %.2f\n",mediana_parzysta(dlugosc,sortowanie(ciag,dlugosc)));
                break;
            }
            else{
                printf("\nMediana tego ciagu to: %d\n",mediana_nieparzysta(dlugosc,sortowanie(ciag,dlugosc)));
                break;
            }
        case 5:
            printf("\nOdchylenie standardowe wynosi: %.2f\n",odchylenie(ciag, dlugosc));
            break;
        case 0:
            powtarzanieMenu=0;
            powtarzanieCalosci=0;
            pytanie=0;
            break;
        default:
            printf("\nTa liczba niczemu nie odpowiada!\n");
            break;
    }
        if(pytanie==1){
        printf("\nCzy chcesz wprowadzić nowy ciag?(1 - tak, 0 - nie)\n");
        scanf("%d",&zmianaCiagu);
        if(zmianaCiagu==1){
            free (ciag);
            powtarzanieMenu=0;
            powtarzanieCalosci=1;
        }
        }
        
    }
    while(powtarzanieMenu==1);
}
    while(powtarzanieCalosci==1);
return 0;
}
