#include <math.h>
int minimum(int tab[],int wielkosc){
    if(wielkosc==1)
        return tab[0];
    else{
        int x;
        x=minimum(tab,wielkosc-1);
        if(x<tab[wielkosc-1])
            return x;
        else
            return tab[wielkosc-1];
    }
}

int max_pomocnicze(int tablica[],int n,int akumulator){
    if (n==0)
        return akumulator;
    else
        return max_pomocnicze(tablica,n-1,tablica[n-1]>akumulator?tablica[n-1]:akumulator);
}

int max(int tablica[],int n){
    return max_pomocnicze(tablica,n-1,tablica[n-1]);
}

float srednia(int liczby[],int n){
    int i,wynik=0;
    for(i=0;i<n;i++){
        wynik+=liczby[i];
    }
    return 1.0*wynik/n;
}

int* sortowanie(int sortowana[],int ileRazy){
    int i,j,zamiana;
    for(i=0;i<ileRazy-1;i++){
        for(j=0;j<ileRazy-i-1;j++){
            if(sortowana[j]>sortowana[j+1]){
            zamiana=sortowana[j];
            sortowana[j]=sortowana[j+1];
            sortowana[j+1]=zamiana;
            }
        }
    }
    return sortowana;
}

int mediana_nieparzysta(int dlugosc, int ciag[]){
    return ciag[dlugosc/2];
}

float mediana_parzysta(int dlugosc, int ciag[]){
    return (ciag[dlugosc/2-1]+ciag[dlugosc/2])/2.0;
}

float odchylenie(int wartosci[],int n){
float wynik=0.0,kwadrat=0.0;
int i;
for(i=0;i<n;i++){
    wynik+=wartosci[i];
    kwadrat+=wartosci[i]*wartosci[i];
}
return sqrt((n*((kwadrat/n)-(wynik/n)*(wynik/n)))/(n-1));
}
