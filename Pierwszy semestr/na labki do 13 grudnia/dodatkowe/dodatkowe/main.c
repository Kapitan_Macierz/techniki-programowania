n//
//  main.c
//  dodatkowe
//
//  Created by Łukasz Michalak on 04/12/2018.
//  Copyright © 2018 Łukasz Michalak. All rights reserved.
//
#include <stdio.h>
#include <stdlib.h>
#include <malloc/malloc.h>

struct czynniki{
    int libczaPierwsza;
    int wielokrotnosc;
};

int main()
{
    int liczba,dzielenie=2,podstawowaWielkosc=1,licznik=1,pierwszyRaz=1;
    struct czynniki *przechowalnia=malloc(podstawowaWielkosc * sizeof(*przechowalnia));
    printf("Prosze podac liczbe do rozlozenia na czynniki pierwsze: ");
    scanf("%d",&liczba);
    if (liczba==0)
        printf("Liczba zero ma nieskonczenie wiele dzielnikow!\n");
    else if (liczba==1)
        printf("Jeden to jeden, nic więcej!\n");
    else{
        while(liczba!=1){
            if(pierwszyRaz==1){
                przechowalnia[podstawowaWielkosc-1].libczaPierwsza=dzielenie;
                przechowalnia[podstawowaWielkosc-1].wielokrotnosc=0;
                pierwszyRaz=0;
            }
            if(liczba%dzielenie!=0){
                pierwszyRaz=1;
                dzielenie++;
                podstawowaWielkosc++;
            }
            else{
                liczba=liczba/dzielenie;
                if (licznik==1){
                przechowalnia[podstawowaWielkosc-1].wielokrotnosc+=1;
                }
            }
        }
        int i;
        for (i=0;i<podstawowaWielkosc;i++)
            if(przechowalnia[i].wielokrotnosc!=0){
                printf("Dzielnik %d jest wielokrotnosci %d\n",przechowalnia[i].libczaPierwsza,przechowalnia[i].wielokrotnosc);
            }
        free(przechowalnia);
    }
    return 0;
    
}

