
#ifndef funkcje_zadania_nr_3_h
#define funkcje_zadania_nr_3_h
float srednia(int liczby[],int n);
int minimum(int tab[],int wielkosc);
int max(int tablica[],int n);
int max_pomocnicze(int tablica[],int n,int akumulator);
int* sortowanie(int sortowana[],int ileRazy);
int mediana_nieparzysta(int dlugosc,int ciag[]);
float mediana_parzysta(int dlugosc,int ciag[]);
float odchylenie(int wartosci[],int n);
#endif /* funkcje_zadania_nr_3_h */
