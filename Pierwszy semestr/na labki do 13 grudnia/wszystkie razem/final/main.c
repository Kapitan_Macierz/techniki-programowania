// Łukasz Michalak  03/12/2018

#include <stdio.h>
#include "glowny.h"

int main() {
    int powtarzanieMenu=1;
    do{
    int legenda;
    printf("\nProsze wybrac zadanie zgodnie z legenda:\n");
    printf("1 - Program wyznaczajacy najmniejsze liczby dodatnie, która sa reprezentowane w arytmetykach zmiennoprzecinkowych\n");
    printf("2 - Obliczanie wartosci wielomianu na dwa różne sposoby\n");
    printf("3 - Wyznaczanie podsawowych statystyk ciagu liczb całkowitych\n");
    printf("4 - Obliczanie wartosci wielomianu n-tego stopnia\n");
    printf("5 - Rozkład liczby na czynniki pierwsze\n");
    printf("6 - Wyznaczanie wszystkich liczb pierwszych mniejszych od podanej liczby naturalnej\n");
    printf("0 - Zakonczenie programu\n");

    scanf("%d",&legenda);
    switch (legenda) {
        case 1:
            zadanie1();
            break;
        case 2:
            zadanie2();
            break;
        case 3:
            zadanie3();
            break;
        case 4:
            zadanie4();
            break;
        case 5:
            zadanie5();
            break;
        case 6:
            zadanie6();
            break;
        case 0:
            powtarzanieMenu=0;
            break;
        default:
            printf("\nPodany numer jest niewlasciwy!\n");
    }
    }
    while(powtarzanieMenu==1);
    return 0;
}
