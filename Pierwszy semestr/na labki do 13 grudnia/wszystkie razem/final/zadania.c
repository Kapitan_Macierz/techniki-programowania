#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include "funkcje zadania nr 3.h"
#include <malloc/malloc.h>
void zadanie1(void){
    printf("\nNajmniejsze liczby reprezentowane w arytmetykach zmiennoprzecikowych to dla:\n");
    printf("%7s %e\n%7s %e\n","Float:",FLT_MIN,"Double:",DBL_MIN);
}
void zadanie2(void){
    int n;
    float i=0.99,tablica[6];
    printf("\nWartosci tego wielomianu obliczone odpowiednio sposobem pierwszym i drugim to:\n");
    for (n=0;n<5;n+=2){
        tablica[n]=(i-1)*(i-1)*(i-1)*(i-1)*(i-1)*(i-1)*(i-1); //liczenie na dwa sposoby i wypisywanie
        tablica[n+1]=i*i*i*i*i*i*i-7*i*i*i*i*i*i+21*i*i*i*i*i-35*i*i*i*i+35*i*i*i-21*i*i+7*i-1;
        printf("- dla %.2f to %17.14f  i %17.14f\n",i,tablica[n],tablica[n+1]);
        i+=0.01;
    }
}
void zadanie3(){
    int i,dlugosc,*ciag,akcja,powtarzanieMenu=1,powtarzanieCalosci=1,zmianaCiagu=0,pytanie=1;
    do{
        powtarzanieCalosci=1;
        printf("\nProsze podac liczbe wyrazow ciagu: ");
        scanf("%d",&dlugosc);
        ciag=malloc(dlugosc * sizeof(*ciag));
        printf("Prosze podac wyrazy ciągu: \n");
        for(i=0;i<dlugosc;i++){
            scanf("%d",&ciag[i]);
        }
        do{
            powtarzanieMenu=1;
            printf("\nProsze wybrac ktora akcja ma byc przeprowadzona na podanym ciagu: \n");
            printf("1 - wyznaczenie minimum\n2 - wyznaczenie maksimum\n3 - obliczanie sredniej\n4 - wyznaczenie mediany\n5 - obliczanie odchylenia standardowego\n0 - wyjscie\n");
            scanf("%d",&akcja);
            switch (akcja) {
                case 1:
                    printf("\nMinimum tego ciagu to: %d\n",minimum(ciag, dlugosc));
                    break;
                case 2:
                    printf("\nMaksimum tego ciagu to: %d\n",max(ciag, dlugosc));
                    break;
                case 3:
                    printf("\nSrednia tego ciagu to: %.2f\n",srednia(ciag, dlugosc));
                    break;
                case 4:
                    if(dlugosc%2==0){
                        printf("\nMediana tego ciagu to: %.2f\n",mediana_parzysta(dlugosc,sortowanie(ciag,dlugosc)));
                        break;
                    }
                    else{
                        printf("\nMediana tego ciagu to: %d\n",mediana_nieparzysta(dlugosc,sortowanie(ciag,dlugosc)));
                        break;
                    }
                case 5:
                    printf("\nOdchylenie standardowe wynosi: %.2f\n",odchylenie(ciag, dlugosc));
                    break;
                case 0:
                    powtarzanieMenu=0;
                    powtarzanieCalosci=0;
                    pytanie=0;
                    break;
                default:
                    printf("\nTa liczba niczemu nie odpowiada!\n");
                    break;
            }
            if(pytanie==1){
                printf("\nCzy chcesz wprowadzić nowy ciag?(1 - tak, 0 - nie)\n");
                scanf("%d",&zmianaCiagu);
                if(zmianaCiagu==1){
                    free (ciag);
                    powtarzanieMenu=0;
                    powtarzanieCalosci=1;
                }
            }
            
        }
        while(powtarzanieMenu==1);
    }
    while(powtarzanieCalosci==1);
}
void zadanie4(void){
    int stopien=0,i,n=0;
    float x,wynik=0,*wspolczynniki;
    printf("\nProsze podac stopien wielomianu: ");
    scanf("%d",&stopien);
    wspolczynniki=malloc(stopien+1 * sizeof(*wspolczynniki)); //tablica ktora dostosowuje swoja dłguość do stopnia wielomianu podanego przez urzytkowanika
    printf("Prosze podac argument rzeczywisty x: ");
    scanf("%f",&x);
    printf("Prosze podac kolejne wspolczynniki wielomianu w kolejnosci od tego przy najwiekszej potedze:\n");
    for(i=0;i<stopien+1;i++)
    {
        scanf("%f",&wspolczynniki[i]); //wpisywanie wspolczynnikow
    }
    i=0;
    while(n<stopien)  //obliczanie wartosci wielomianu wedlug wzoru (((ax+b)x+c)x+d)x+e itd
    {
        if(wynik==0)
        {
            wynik=wspolczynniki[i]*x+wspolczynniki[i+1];
            i=i+2;
            n++;
        }
        else
        {
            wynik=wynik*x+wspolczynniki[i];
            i++;
            n++;
        }
    }
    free(wspolczynniki); //czyszczenie
    if (stopien==0)
        printf("Wynik to %.2f\n",wspolczynniki[0]);
    else
        printf("Wynik to %.2f\n",wynik);
}
void zadanie5(void){
    int liczba,dzielenie=2;
    printf("\nProsze podac liczbe do rozlozenia na czynniki pierwsze: ");
    scanf("%d",&liczba);
    if (liczba==0)
        printf("Liczba zero ma nieskonczenie wiele dzielnikow!\n");
    else if (liczba==1)
        printf("Jeden to jeden, nic więcej!\n"); //sprawdzenie specyficznych libcz 1 i 0
    else{
        while(liczba!=1){
            if(liczba%dzielenie!=0) // jak reszta równa zero to podzielna i dzielimy jak nie to zwiekszamy dzielnik
                dzielenie++;
            else{
                liczba=liczba/dzielenie;
                printf("%d\n",dzielenie);
            }
        }
    }
}
void zadanie6(void){
    int liczbaUzytkownika=1,k,i,dzielnik=2,numerTablicy=2,*tablicaLiczb;
    printf("Prosze podać liczbe do której wypisane zostana liczby pierwsze: ");
    scanf("%d",&liczbaUzytkownika); //prosi o ograniczenie z góry
    tablicaLiczb=malloc(liczbaUzytkownika+1 * sizeof(*tablicaLiczb)); //tablica z liczbami do sprawdzania która ma wielkosc zalezna od ograniczenia z góry uzytkownika
    for(i=0;i<=liczbaUzytkownika;i++) /*generalnie to dziala tak ze jest tworzona tablica wielkosci podanej przez uzytkownika i wypełnia ja
                                       liczbami po kolei, potem dzieli kolejne wyrazy zaczynajac od 2 i gdzy reszta z dzielenia jest rowna zero to znaczy ze liczba jest podzielna,
                                       nie jest pierwsza wiec zeby odróznić podzielne to wszystkim podzielnim przypisuje wartosc 0, gdy skonczy to leci od nowa ze sprawdzaniem
                                       czy liczby sa podzielne i podstawia zera tylko tym razem dzieli przez najmniejsza liczbe w tablicy ktora nie jest zerem, wszystko konczy sie
                                       na liczbie ograniczajacej */
    {
        tablicaLiczb[i]=i;
    }
    for(i=0;i<liczbaUzytkownika;i++){
        numerTablicy=2+i;  //reset numer elementu znowu do poczatku +1
        if(tablicaLiczb[numerTablicy]!=0){ //jesli wartosc tablicy dla nastepnego numeru nie jest 0 to jest to teraz przez nia musimy dzielic
            dzielnik=numerTablicy; //zamienia dzielnik na kolenja liczbe pierwszą
            printf("%d\n",dzielnik);
        }
        for(k=2;k<liczbaUzytkownika;k++)
        {
            if(tablicaLiczb[numerTablicy]%dzielnik==0){ // jesli reszta z dzielenia jest równa zero dla elementu to jest podzielny i to nie jest pierwsza liczba
                tablicaLiczb[numerTablicy]=0; //przypisuje temu elementowi wartosc 0 zeby go odróznić
                numerTablicy++;
            }
            else{
                numerTablicy++;
            }
        }
    }
    free(tablicaLiczb);
}
