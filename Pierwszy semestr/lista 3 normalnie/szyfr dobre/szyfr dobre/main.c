
#include <stdio.h>
#include <stdlib.h>
#define N 2000

void kodowanie(char literki[],int przesuniecie);
void dekodowanie(char literki[],int przesuniecie);


int main(int argc,char* argv[]) {
    char literki[N];
    FILE* szyfr;
    szyfr=fopen(argv[1],"r");
    if(szyfr==0){
        printf("Nie udalo sie odnalezc pliku z tekstem!\n");
        return 0;
    }
    fgets(literki, N, szyfr);
    fclose(szyfr);
    int co=0;
    do{
    printf("Chcesz kodowac (1) czy dekodowac (2) tekst, który zostal wczytany?\n");
    scanf("%d",&co);
    if(co==1){
        kodowanie(literki,atoi(argv[3]));
        co=-1;
    }
    if(co==2){
        dekodowanie(literki,atoi(argv[3]));
        co=-1;
    }
    if(co>2||co<-1){
        printf("Prosze wybrac 1 lub 2!\n");
    }
    }while(co!=-1);
    FILE *plik = fopen(argv[2],"w");
    int i;
    for(i=0;i<N;i++){
        printf("%c",literki[i]);
        fprintf(plik,"%c",literki[i]);
    }
    fclose(plik);
    puts("\n");
    return 0;
}

void kodowanie(char literki[],int przesuniecie){
    int i;
    for(i=0;i<N;i++){
        if(literki[i]>=65 && literki[i]<=90-przesuniecie){
            literki[i]=(int)(literki[i])+przesuniecie;
        } //przesówanie wielkich liter
        else if(literki[i]>=91-przesuniecie && literki[i]<=90){
            literki[i]=(int)(literki[i])-26+przesuniecie;
        }// wielkie litery które by wyjechały za przedział i musza leciec od poczatku
        else if(literki[i]>=97 && literki[i]<=122-przesuniecie){
            literki[i]=(int)(literki[i])+przesuniecie;
        }//małe liter
        else if(literki[i]>=123-przesuniecie && literki[i]<=122){
            literki[i]=(int)(literki[i])-26+przesuniecie; //anagloicznie
        }
    }
}
void dekodowanie(char literki[],int przesuniecie){
    int i;
    for(i=0;i<N;i++){
        if(literki[i]>=65+przesuniecie && literki[i]<=90){
            literki[i]=(int)(literki[i])-przesuniecie;
        } //przesówanie wielkich liter
        else if(literki[i]>=65 && literki[i]<=90-26+przesuniecie){
            literki[i]=(int)(literki[i])+26-przesuniecie;
        }// wielkie litery które by wyjechały za przedział i musza leciec od poczatku
        else if(literki[i]>=97+przesuniecie && literki[i]<=122){
            literki[i]=(int)(literki[i])-przesuniecie;
        }//małe liter
        else if(literki[i]>=97 && literki[i]<=122-26+przesuniecie){
            literki[i]=(int)(literki[i])+26-przesuniecie; //anagloicznie
        }
    }
}
