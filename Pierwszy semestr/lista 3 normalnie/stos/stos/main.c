//
//  Łukasz Michalak 15/01/2019
//


#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <malloc.h>

struct wezel{
    int liczba;
    struct wezel* nastepny;
};

void odkladanie(struct wezel** strzalka,int liczba);
void czytanie(struct wezel* wskaznik);
void niszczenie_calego(struct wezel** strzalka);
void usuwanie(struct wezel** strzalka);
void wyswietlanie_calego(struct wezel* wskaznik);
void nawiasy(struct wezel** strzalka,char* napis,int wielkosc);


int main(int argc, const char * argv[]) {
    struct wezel* wskaznik=NULL;
    struct wezel* nowy=NULL;
    char* napis;
    int wielkosc=15,decyzja=42,menu,dane;
    do{
    puts("\n-------- Menu --------");
    puts("0 - zakonczenie programu\n1 - odkladanie na stos\n2 - odczytywanie ze stosu\n3 - zdejmowanie ze stosu\n4 - niszczenie stosu\n5 - wyswietlanie calego stosu\n6 - sprawdzanie nawiasow w wyrazeniu\n");
    scanf("%d",&menu);
    switch (menu) {
        case 1:
            system("cls");
            puts("\nProsze podac liczbe do odlzenia na stosie: ");
            scanf("%d",&dane);
            odkladanie(&wskaznik,dane);
            puts("\nDodano liczbe!\n");
            break;
        case 2:
            system("cls");
            czytanie(wskaznik);
            break;
        case 3:
            system("cls");
            usuwanie(&wskaznik);
            puts("\nUsunieto liczbe!\n");
            break;
        case 4:
            system("cls");
            niszczenie_calego(&wskaznik);
            puts("\nStos zniszczony!\n");
            break;
        case 5:
            system("cls");
            puts(" ");
            wyswietlanie_calego(wskaznik);
            break;
        case 6:
            system("cls");
            do{
            puts("\nCzy chcesz zmienic ilosc znakow możliwych do wpisania do sprawdzenia poprawnosci nawiasów?\nPodstawowa wielkosc to 15 znakow. Po tej liczbie nie bedzie nic sprawdzane!\n[1 - TAK,0 - NIE]\n");
            scanf("%d",&decyzja);
            if(decyzja==0){}
            else if(decyzja==1){
                puts("\nProsze podac ilosc znakow\n");
                scanf("%d",&wielkosc);
            }
            else{
                system("cls");
                puts("\nTo nie byla liczba 0 lub 1! Prosze uwazac przy wpisywaniu.\n");
            }
            }while(decyzja!=1&&decyzja!=0);
            napis=malloc(wielkosc *sizeof(napis));
            puts("Prosze podac napis:\n");
            getchar();
            gets(napis);
            nawiasy(&nowy,napis,wielkosc);
            break;
        case 0:
            return 0;
        default:
            system("cls");
            puts("\nProsze podac liczbe ze zbioru w legendzie!\n");
            break;
    }
    }while(1);
}
void odkladanie(struct wezel** strzalka,int x){
    if((*strzalka)==NULL){
        (*strzalka)=malloc(sizeof(struct wezel));
        (*strzalka)->liczba=x;
        (*strzalka)->nastepny=NULL;
    }
    else{
        odkladanie(&(*strzalka)->nastepny, x);
    }
}
void czytanie(struct wezel* wskaznik){
    
    if((wskaznik)!=NULL){
        if((*wskaznik).nastepny==NULL){
            printf("\n%d\n",(*wskaznik).liczba);
        }
    else{
        czytanie((*wskaznik).nastepny);
    }
    }
    else{
        puts("\nStos jest pusty!\n");
    }
}
void niszczenie_calego(struct wezel** strzalka){
    if(*strzalka!=NULL){
        niszczenie_calego(&(*strzalka)->nastepny);
        free((*strzalka));
        (*strzalka)=NULL;
    }
}
void usuwanie(struct wezel** strzalka){
    if(*strzalka!=NULL){
        if((strzalka)!=NULL){
            if((*strzalka)->nastepny==NULL){
                free((*strzalka));
                (*strzalka)=NULL;
            }
            else{
                usuwanie(&(*strzalka)->nastepny);
            }
        }
    }
}
void wyswietlanie_calego(struct wezel* wskaznik){
    if(wskaznik!=NULL){
            printf("%d ",(*wskaznik).liczba);
            wyswietlanie_calego((*wskaznik).nastepny);
        
    }
    else{
        puts(" ");
        return;
    }
}
void nawiasy(struct wezel** strzalka,char* napis,int wielkosc){
    int i;
    for(i=0;i<wielkosc;i++){
        if(napis[i]=='('){
            odkladanie(strzalka,1);
        }
        else if(napis[i]==')'&&*strzalka==NULL){
            odkladanie(strzalka,1);
        }
        else if(napis[i]==')'&&*strzalka!=NULL){
            usuwanie(strzalka);
        }
    }
    if(*strzalka!=NULL){
        puts("\nNawiasy w wyrazeniu sa źle wstawione!\n");
    }
    else{
        puts("\nNawiasy są poprawnie wstawione! :)\n");
    }
    niszczenie_calego(strzalka);
}
