//
//Łukasz Michalak 17/01/2019.
//

#include <stdio.h>
#include <stdlib.h>

void sortowanie_na_malych(int *tab, int poczatek, int koniec);
void swap(int *tab, int a, int b);
void sortowanko(int *tab, int *posortowana, int poczatek, int koniec);


int main(){
    int *tab;
    int *posortowana;
    int dlugosc, i;
    printf("Prosze podac ilosc znakow tablicy: ");
    scanf("%d",&dlugosc);
    tab=malloc(dlugosc* sizeof(*tab));
    posortowana=malloc(dlugosc* sizeof(*posortowana));
    for(i=0; i<dlugosc; i++){
        printf("Podaj %d. wyraz ciagu: ",i+1);
        scanf("%d",&tab[i]);
    }
    if(dlugosc<=5 && dlugosc>0){
        sortowanie_na_malych(tab, 0, dlugosc-1);
        for(i=0; i<dlugosc; i++)
            printf("%d \n", tab[i]);
    }
    else if(dlugosc>5){
        sortowanko(tab, posortowana, 0, dlugosc-1);
        printf("\nPosortowane liczby: \n");
        for(i=0; i<dlugosc; i++)
            printf("%d\n", posortowana[i]);
    }
    else{
        printf("Nieprawidlowe dane\n");
    }
    free(tab);
    free(posortowana);
    return 0;
}
void swap(int *tab, int a, int b){
    int zamiana;
    zamiana=tab[a];
    tab[a]=tab[b];
    tab[b]=zamiana;
}
void sortowanie_na_malych(int *tab, int poczatek, int koniec){
    if((koniec-poczatek)==1){
        if(tab[poczatek]>tab[koniec]){
            swap(tab, poczatek, koniec);
        }
    }
    if((koniec-poczatek)==2){
        if(tab[poczatek]>tab[koniec]){
            swap(tab, poczatek, koniec);
        }
        if(tab[poczatek+1]>tab[koniec]){
            swap(tab, poczatek+1, koniec);
        }
        if(tab[poczatek]>tab[poczatek+1]){
            swap(tab, poczatek, poczatek+1);
        }
    }
    if((koniec-poczatek)==3){
        if(tab[poczatek]>tab[poczatek+1]){
            swap(tab, poczatek, poczatek+1);
        }
        if(tab[koniec-1]>tab[koniec]){
            swap(tab, koniec-1, koniec);
        }
        if(tab[poczatek+1]>tab[koniec]){
            swap(tab, poczatek+1, koniec);
        }
        if(tab[poczatek]>tab[koniec-1]){
            swap(tab, poczatek, koniec-1);
        }
        if(tab[poczatek+1]>tab[koniec-1]){
            swap(tab, poczatek+1, koniec-1);
        }
        
    }
    if((koniec-poczatek)==4){
        if(tab[poczatek]>tab[poczatek+1]){
            swap(tab, poczatek, poczatek+1);
        }
        if(tab[poczatek+2]>tab[poczatek+3]){
            swap(tab, poczatek+2, poczatek+3);
        }
        if(tab[poczatek+1]>tab[poczatek+3]){
            swap(tab, poczatek+1, poczatek+3);
            swap(tab, poczatek, poczatek+2);
        }
        if(tab[koniec]>tab[poczatek+1]){
            if(tab[poczatek+3]>tab[koniec]){
                swap(tab, poczatek+3, koniec);
            }
        }
        else{
            if(tab[poczatek]>tab[koniec]){
                swap(tab, poczatek+3, koniec);
                swap(tab, poczatek+1, poczatek+3);
                swap(tab, poczatek, poczatek+1);
            }
            else{
                swap(tab, poczatek+3, koniec);
                swap(tab, poczatek+1, poczatek+3);
            }
            
        }
        if(tab[poczatek+1]>tab[poczatek+2]){
            if(tab[poczatek]>tab[poczatek+2]){
                swap(tab, poczatek+1, poczatek+2);
                swap(tab, poczatek, poczatek+1);
            }
            else{
                swap(tab, poczatek+1, poczatek+2);
            }
        }
        else
            if(tab[poczatek+2]>tab[poczatek+3]){
                swap(tab,poczatek+2,poczatek+3);
            }
    }
}
void sortowanko(int *tab, int *posortowana, int poczatek, int koniec){
    if (koniec<=poczatek){
        return;
    }
    if((koniec-poczatek)<5){
        sortowanie_na_malych(tab, poczatek, koniec);
        return;
    }
    int mid=(poczatek+koniec) / 2;
    int lewy=poczatek;
    int prawy=mid+1;
    int counter;
    sortowanko(tab, posortowana, poczatek, mid);
    sortowanko(tab, posortowana, mid+1, koniec);
    
    for (counter=poczatek;counter<=koniec;counter++){
        if (lewy==mid+1){
            posortowana[counter]=tab[prawy];
            prawy++;
        } else if (prawy==koniec+1) {
            posortowana[counter]=tab[lewy];
            lewy++; //konce tablic
        } else if (tab[lewy]<tab[prawy]) {
            posortowana[counter]=tab[lewy];
            lewy++;
        } else {
            posortowana[counter]=tab[prawy];
            prawy++; //reszta
        }
    }
    for (counter=poczatek; counter<=koniec;counter++) {
        tab[counter]=posortowana[counter];
    }
}
