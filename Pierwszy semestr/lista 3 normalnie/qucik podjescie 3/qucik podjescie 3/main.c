//
//  main.c
//  qucik podjescie 3
//
//  Created by Łukasz Michalak on 07/01/2019.
//  Copyright © 2019 Łukasz Michalak. All rights reserved.
//
#include <stdio.h>
#include <stdlib.h>
#include <malloc/malloc.h>

void sortowanie(int *ciag,int lewyKres,int prawyKres);
int magiczne_zamienianie(int *ciag,int lewyKres,int prawyKres);

int main(){
    int dlugosc=0,i;
    int*ciag;
    printf("Prosze podac dlugosc ciagu liczb: ");
    scanf("%d",&dlugosc);
    ciag=malloc(dlugosc *sizeof(*ciag));
    printf("Prosze podac kolejne wyrazu ciagu: \n");
    for(i=0;i<dlugosc;i++){
        scanf("%d",&ciag[i]);
    }
    sortowanie(ciag,0,dlugosc-1);
    printf("Posortowany ciag wyglada tak: ");
    for(i=0;i<dlugosc;i++){
        printf("%d ",ciag[i]);
    }
    printf("\n");
    free(ciag);
    return 0;
}
void sortowanie(int *ciag,int lewyKres,int prawyKres){
    if(lewyKres>=prawyKres){
        return;
    }
    int srodek;
    srodek=magiczne_zamienianie(ciag,lewyKres,prawyKres);
    sortowanie(ciag, lewyKres, srodek-1);
    sortowanie(ciag, srodek+1, prawyKres);
}

int magiczne_zamienianie(int *ciag,int lewyKres,int prawyKres){
    int zamiana;
    zamiana=ciag[lewyKres];
    ciag[lewyKres]=ciag[prawyKres];
    ciag[prawyKres]=zamiana;
    int i=lewyKres,j=lewyKres;
    for(;i<prawyKres+1;i++){
        if(ciag[i]<ciag[prawyKres]){
            zamiana=ciag[i];
            ciag[i]=ciag[j];
            ciag[j]=zamiana;
            j++;
        }
    }
    zamiana=ciag[j];
    ciag[j]=ciag[prawyKres];
    ciag[prawyKres]=zamiana;
    return j;
}
