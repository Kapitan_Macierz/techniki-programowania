//
//  main.c
//  hetman na spokojnie
//
//  Created by Łukasz Michalak on 13/12/2018.
//  Copyright © 2018 Łukasz Michalak. All rights reserved.
//
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc/malloc.h>
#include <unistd.h>

#define N 100

struct rozwiazania{
    int wiersz;
    int kolumna;
};
struct pola{
    int miejsce[N][N];
    struct rozwiazania hetman[N];
};
int szukanie_pozycj(int ktory_hetman,struct pola *plansza,int wielkosc,int decyzja);
void bicie(struct pola *plansza,int dlugosc, int ktory_hetman);
void wypelnianie_planszy(struct pola *plansza,int wielkosc);
void restoracja(struct pola *plansza,int wielkosc,int ktory_hetman);
int czy_wygrana(struct pola *plansza,int wielkosc);
void wypisanie_planszy(struct pola *plansza,int wielkosc,int ktory_hetman);

int main(int argc,char* argv[]){
    struct pola plansza;
    int wielkosc,g=1,decyzja;
    wielkosc=atoi(argv[1]);
    if(wielkosc<4){
        printf("Wielkosc planszy musi byc conajmniej 4 zeby algorytm mogl zadzialac!");
        return 0;
    }
    wypelnianie_planszy(&plansza, wielkosc);
    printf("\nMiejsca hetmanow na planszy beda zaznaczone znakiem X\nCzy chcesz zobaczyc animacje rozwiazywania?(UWAGA! Dla duzych tablic moze byc dluga!)\n[1 - Tak,0 - Nie]\n");
    scanf("%d",&decyzja);
    szukanie_pozycj(0, &plansza, wielkosc,decyzja);
    system("clear");
    printf("Wynik prezentuje sie nastepujaco\nWspolrzedne hetmanow to:\n");
    for(g=0;g<wielkosc;g++){
        printf("Hetman nr %d: %d,%d\n",g+1,plansza.hetman[g].kolumna,plansza.hetman[g].wiersz);
    }
    wypisanie_planszy(&plansza, wielkosc,wielkosc);
    return 0;
}
void wypelnianie_planszy(struct pola *plansza,int wielkosc){
    int i,j;
    for(i=0;i<wielkosc;i++){
        for(j=0;j<wielkosc;j++){
            plansza->miejsce[i][j]=1;//wszystkie pola zdolne do gry
        }
    }
}
void bicie(struct pola *plansza,int dlugosc,int ktory_hetman){
    int i,j,k=0;
    for(i=plansza->hetman[ktory_hetman].wiersz;i<dlugosc;i++){
        plansza->miejsce[plansza->hetman[ktory_hetman].wiersz+k][plansza->hetman[ktory_hetman].kolumna]=0;
        k++;//nad hetmanem
    }
    k=0;
    for(i=plansza->hetman[ktory_hetman].wiersz;i>-1;i--){
        plansza->miejsce[plansza->hetman[ktory_hetman].wiersz-k][plansza->hetman[ktory_hetman].kolumna]=0;
        k++;//pod hetmanem
    }
    k=0;
    for(i=plansza->hetman[ktory_hetman].kolumna;i<dlugosc;i++){
        plansza->miejsce[plansza->hetman[ktory_hetman].wiersz][plansza->hetman[ktory_hetman].kolumna+k]=0;
        k++;//na prawo
    }
    k=0;
    for(i=plansza->hetman[ktory_hetman].kolumna;i>-1;i--){
        plansza->miejsce[plansza->hetman[ktory_hetman].wiersz][plansza->hetman[ktory_hetman].kolumna-k]=0;
        k++;//lewo
    }
    k=0;
    for(i=plansza->hetman[ktory_hetman].wiersz,j=plansza->hetman[ktory_hetman].kolumna;i<dlugosc&&j<dlugosc;i++,j++){
        plansza->miejsce[plansza->hetman[ktory_hetman].wiersz+k][plansza->hetman[ktory_hetman].kolumna+k]=0;
        k++;//skos prawo gora
    }
    k=0;
    for(i=plansza->hetman[ktory_hetman].wiersz,j=plansza->hetman[ktory_hetman].kolumna;i>-1&&j>-1;i--,j--){
        plansza->miejsce[plansza->hetman[ktory_hetman].wiersz-k][plansza->hetman[ktory_hetman].kolumna-k]=0;
        k++;//skos lewo dol
    }
    k=0;
    for(i=plansza->hetman[ktory_hetman].wiersz,j=plansza->hetman[ktory_hetman].kolumna;i>-1&&j<dlugosc;i--,j++){
        plansza->miejsce[plansza->hetman[ktory_hetman].wiersz-k][plansza->hetman[ktory_hetman].kolumna+k]=0;
        k++;//skos prawo dol
    }
    k=0;
    for(i=plansza->hetman[ktory_hetman].wiersz,j=plansza->hetman[ktory_hetman].kolumna;i<dlugosc&&j>-1;i++,j--){
        plansza->miejsce[plansza->hetman[ktory_hetman].wiersz+k][plansza->hetman[ktory_hetman].kolumna-k]=0;
        k++;//skos lewo gora
    }
}
void restoracja(struct pola *plansza,int wielkosc,int ktory_hetman){
    wypelnianie_planszy(plansza, wielkosc);
    for(ktory_hetman-=1;ktory_hetman>-1;ktory_hetman--){
    bicie(plansza,wielkosc,ktory_hetman);//stare bicia hetmanow
    }
}
int czy_wygrana(struct pola *plansza,int wielkosc){
    int f,g,sprawdzenie=0;
    for(f=0;f<wielkosc;f++){
        for(g=0;g<wielkosc;g++){
            sprawdzenie++;
        }
    }
    if(sprawdzenie==wielkosc*wielkosc){
        return 1;
    }
    else{
        return 0;
    }
}
int szukanie_pozycj(int ktory_hetman,struct pola *plansza,int wielkosc,int decyzja){
    int i,licznik=0,KONIEC=0,k;
    for(i=0;i<wielkosc;i++){
        if(KONIEC==1){
            return KONIEC;
        }
        else if(ktory_hetman==wielkosc){
            return KONIEC;
        }
        if(plansza->miejsce[i][ktory_hetman]==1){
            plansza->hetman[ktory_hetman].wiersz=i;
            plansza->hetman[ktory_hetman].kolumna=ktory_hetman;//dane nowego hetmana zapisane
            bicie(plansza,wielkosc,ktory_hetman);//zamienia niedozwolone pola na 0
            if(decyzja==1){
                //getchar();
                usleep(700000);
                system("clear");
                printf("\nUstawiamy hetmana na polu %d,%d\n",plansza->hetman[ktory_hetman].kolumna,plansza->hetman[ktory_hetman].wiersz);
                wypisanie_planszy(plansza, wielkosc,ktory_hetman+1);
            }
            KONIEC=szukanie_pozycj(ktory_hetman+1, plansza, wielkosc,decyzja);
            licznik++;
            if(ktory_hetman==wielkosc-1){ //jezeli to ostatni chteman to sprawdz czy to koniec
                KONIEC=czy_wygrana(plansza, wielkosc);
                if(KONIEC==1){
                    return KONIEC;
                }
            }
            if(KONIEC==0){
                restoracja(plansza, wielkosc, ktory_hetman);//odnowa planszy to tego sprzed sprawdzenia hetmana+1
                if(decyzja==1){
                    system("clear");
                    printf("\nNie da sie postawic. Cofamy sie\n");
                    wypisanie_planszy(plansza, wielkosc,ktory_hetman+1);
                }
                for(k=0;k<licznik;k++){
                    plansza->miejsce[plansza->hetman[ktory_hetman].wiersz-k][plansza->hetman[ktory_hetman].kolumna]=0;//zamiania pola juz sprawdzone na 0 zeby nie krecic sie w nieskonczonosc
                }
            }
    }
    }
    return KONIEC;
}
void wypisanie_planszy(struct pola *plansza,int wielkosc,int ktory_hetman){
    int g,f;
    printf("  ");
    for(f=0;f<wielkosc;f++){
        if(f<10){
        printf("  %d ",f);
        }
        else if(f>9){
            printf("  %d",f);
        }
    }
    printf("\n");
    for(g=0;g<ktory_hetman;g++){
        plansza->miejsce[plansza->hetman[g].wiersz][plansza->hetman[g].kolumna]=42;
    }
    printf("  ");
    for(g=0;g<4*wielkosc+1;g++){
        printf("-");
    }
    printf("\n");
    for(f=0;f<wielkosc;f++){
        if(f<10){
            printf("%d ",f);
        }
        else if(f>9){
            printf("%d",f);
        }
        for(g=0;g<wielkosc;g++){
            printf("|");
            if(plansza->miejsce[f][g]==42){
                printf(" X ");
            }
            else{
                printf("   ");
            }
        }
        printf("|");
        printf("\n  ");
        for(g=0;g<4*wielkosc+1;g++){
            printf("-");
        }
        printf("\n");
    }
}
