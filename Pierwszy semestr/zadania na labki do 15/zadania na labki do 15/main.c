//
//  zadania na laboratoria do 15 listopada
//
//  Łukasz Michalak 06/11/2018.
//

#include <stdio.h>
#include <stdlib.h>

void tableka()
{
    char a='c',b='h',c='g',d='e',e='k'; // deklaracja zmiennych ktore przechowuja symbole stalych
    float A=299792458,B=0.0000000000000000000000000000000006626068,C=9.81152341,D=0.0000000000000000001602176,E=0.000000000000000000000013806504; // deklaracja zmiennych ktore przechowuja wartosci stalych
    printf("Tabela stalych fizycznych:\n"); // tabelka
    printf("Nazwa\tWartosc\t\t\tJednostka\tOpis\n");
    printf("%c%19e\tm/s\t\t\tPredkosc swiatla\n",a,A);
    printf("%c%19e\tJ*s\t\t\tStala Planca\n",b,B);
    printf("%c%13.4f\t\t\tm/s^2\t\tPrzspieszenie ziemskie we Wroclawiu\n",c,C);
    printf("%c%19e\tC\t\t\tLadunek elementary\n",d,D);
    printf("%c%19e\tJ/K\t\t\tStala Boltzmanna\n\n",e,E);
}

void wielomian()
{
    float wspolczynniki[5],x,wynik; //deklaracja tablicy przechowywujacej wspolczynniki wielomianu i zmiennej wartosci x oraz wyniku
    printf("Prosze o podanie pieciu kolejnych wspolczynnikow wielomianu p w kolejnosci od wspolczynnika przy najwiekszej potedze:\n");
    int i;
    for (i=0;i<5;i++)
    {
        scanf("%f",&wspolczynniki[i]);//zapisuje w tablicy kolejno 5 wposlczynnikow
    }
    printf("Prosze podac wartosc x: \n"); // teraz czas na x
    scanf("%f",&x);
    wynik=((((wspolczynniki[0]*x+wspolczynniki[1])*x+wspolczynniki[2])*x+wspolczynniki[3])*x+wspolczynniki[4]); // obliczanie wyniku optymalnym sposobem
    printf("Wartosc wielomianu p(x) jest rowna %.2f\n\n",wynik);
}

void stopnie()
{
    int powtarzanie=1; // zmienna uzyta do zapetlenia
    do
    {
        printf("Prosze podac temperature w stopniach Fahrenheita: ");
        float tempF,tempC; //zmienne do wykonania przeliczenia
        scanf("%f",&tempF);
        tempC=(tempF-32)/1.8;
        if (tempC>-273.15) //sprawdzenie czy wynik jest mniejszy od zera absolutnego
        {
            printf("Ta temperatura w stopniach Celsjusza to %.2f\n\n",tempC);
            powtarzanie=0; //wynik byl wiekszy od zera absolutnego wiec petla nie musi byc wykoana
        }
        else
        {
            printf("Podana temperatura jest mniejsza od zera absolutnego!\n"); //upomnienie urzytkownika o ponowne wpisanie temperatury ktora bedze wieksza od zera absolutnego
        }
    }
    while (powtarzanie==1);
}

void sortowanieNIEbabelkowe()
{
    int takaSobieWielkaPetla=1;
do
{
    int a,b,c,d,e,zamiana;
    printf("Prosze podac piec roznych liczb calkowitych do posortowania: \n");
    scanf("%d",&a);
    scanf("%d",&b);
    scanf("%d",&c);
    scanf("%d",&d);
    scanf("%d",&e);
if(a==b||a==c||a==d||a==e||b==c||b==d||b==e||c==d||c==e||d==e)//sprawdzenie czy liczby sa rozne
{
    printf("Któres z podanych liczb sa sobie rowne!\n");
}
else
{
    if((a-b)*(b-c)>0)//jak dodatnie to oba czynniki albo zaden
    {//uporzatkowanie 3 liczb
        if(a<c)
        {
            zamiana=a;
            a=c;
            c=zamiana;
        }
    }
    else
    {
        if(a>b)
        {
            if(a>c)
            {
                zamiana=c;
                c=b;
                b=zamiana;
            }
            else
            {
                zamiana=a;
                a=c;
                c=zamiana;
                zamiana=b;
                b=c;
                c=zamiana;
            }
        }
        else
        {
            if(a>c)
            {
                zamiana=a;
                a=b;
                b=zamiana;
            }
            else
            {
                zamiana=a;
                a=b;
                b=zamiana;
                zamiana=c;
                c=b;
                b=zamiana;
            }
        }
    } // 3 liczby sa posortowane tak ze zawsze a>b>c

    if((d-a)*(d-b)*(d-c)>0) //jak dodatnie to albo wszystkie czynniki albo 1 z 3 i mamy 2 ujemne
    {//odpowiednie dodanie czwartej liczby do nierownosci
     if(d>b)
     {
         zamiana=a;
         a=d;
         d=zamiana;
         zamiana=b;
         b=d;
         d=zamiana;
         zamiana=c;
         c=d;
         d=zamiana;
     }
     else
     {
         zamiana=c;
         c=d;
         d=zamiana;
     }
    }
    else
    {
        if(d>b)
        {
            zamiana=b;
            b=d;
            d=zamiana;
            zamiana=c;
            c=d;
            d=zamiana;
        }
    } // czwarta liczba jest dodana tak ze zawsze a>b>c>d
    
    if((e-a)*(e-b)*(e-c)*(e-d)>0)//jak dodatnie to wszystkie czynniki albo 2 najwieksze albo zaden
    {//odpowiednio dodana piata liczba
        if(e<c)
        {
            printf("Twoje liczby w kolejnosci rosnacej prezentuja sie tak: %d %d %d %d %d\n\n",e,d,c,b,a);//a>b>c>d>e
            takaSobieWielkaPetla=0;
        }
        else if(e<b)
        {
            printf("Twoje liczby w kolejnosci rosnacej prezentuja sie tak: %d %d %d %d %d\n\n",d,c,e,b,a);//a>b>e>c>d
            takaSobieWielkaPetla=0;
        }
        else
        {
            printf("Twoje liczby w kolejnosci rosnacej prezentuja sie tak: %d %d %d %d %d\n\n",d,c,b,a,e);//e>a>b>c>d
            takaSobieWielkaPetla=0;
        }
    }
    else
    {
        if(e>b)
        {
            printf("Twoje liczby w kolejnosci rosnacej prezentuja sie tak: %d %d %d %d %d\n\n",d,c,b,e,a);//a>e>b>c>d
            takaSobieWielkaPetla=0;
        }
        else
        {
            printf("Twoje liczby w kolejnosci rosnacej prezentuja sie tak: %d %d %d %d %d\n\n",d,e,c,b,a);//a>b>c>e>d
            takaSobieWielkaPetla=0;
        }
    }
}
}
    while(takaSobieWielkaPetla==1);
}

int main()
{
    int numerZadania,petla=1;
    while(petla==1) //peta pozwala na wielkorotny wyobr zadan dopoki urzytkownik sobie nie zyczy przestac
    {
        printf("Prosze wybrac od 0 do 4:\n0. Zakonczenie programu\n1. Wypisywanie na ekranie tabeli (zad 1)\n2. Przeliczanie temperatury (zad 2)\n3. Sortowanie 5 liczb (zad 3)\n4. Obliczanie wartosci wielomianu (zad 4)\n");
        scanf("%d",&numerZadania);
        switch(numerZadania) //poszczegolne zadania przywolywne sa przez funkcje
        {
            case 0:
                petla=0;
                break;
            case 1:
                tableka();
                break;
            case 2:
                stopnie();
                break;
            case 3:
                sortowanieNIEbabelkowe();
                break;
            case 4:
                wielomian();
                break;
            default:
                printf("Bledny numer zadania!\n\n");
                break;
   
        }
    }
    return 0;
}
